% passive liquidity
idxPos = tradeTable.passiveLiquidity .* tradeTable.signal > 0;

idxNull = tradeTable.passiveLiquidity .* tradeTable.signal == 0;

idxNeg = tradeTable.passiveLiquidity .* tradeTable.signal < 0;


disp(['event condition: PL*signal > 0',' event occurence: ', num2str(sum(idxPos)), '   avg pnl: ', num2str(sum(tradeTable.pnl(idxPos))/sum(idxPos)) ]);


disp(['event condition: PL*signal = 0',' event occurence: ', num2str(sum(idxNull)), '   avg pnl: ', num2str(sum(tradeTable.pnl(idxNull))/sum(idxNull)) ]);


disp(['event condition: PL*signal < 0',' event occurence: ', num2str(sum(idxNeg)), '   avg pnl: ', num2str(sum(tradeTable.pnl(idxNeg))/sum(idxNeg)) ]);


totalLiquidity = tradeTable.passiveLiquidity + double(tradeTable.tradeImb);

directionalTotalLiquidity = totalLiquidity .* tradeTable.signal;

scatter(directionalTotalLiquidity, tradeTable.pnl);