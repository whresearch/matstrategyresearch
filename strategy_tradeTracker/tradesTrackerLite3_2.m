classdef tradesTrackerLite3_2 < StrategyLite
    
    % lbw = 1 ; ioc in
    % signal and trailing pnl determine wether ioc or pnl out
    % diff from tradesTrackerLite2: use bidrate to determine when to start
    % limit out
    properties
        run_mode
        numTimeOutIOCOrders   = 0;
        numPnLFilterIOCOrders = 0;
        % internal parameters 
        lbw = 8;                   
        lbw_bid 
        lbw_ask 
        lbw_bidSize 
        lbw_askSize 
        %lbw_volume = nan(lbw,1);
        %lbw_vwap = nan(lbw,1);
        lbw_sizeBuy 
        lbw_sizeSell 
        lbw_PL_ask
        lbw_PL_bid        % passive liquidity 
        
        tickSize
        sessionEnd = false;
           
        %signalThreshold      = 7 * tickSize;
        
        %takeprofitLevel      = 24 * tickSize;
        %stoplossLevel        = -7 * tickSize;
        
        %strategy parameters
        trailingStoplossLevel 

        tradeImbThreshold
        
        tradeImbThreshold2
        
        bidRatioThreshold
        
        shortSizeThreshold
        
        passiveImbThreshold
        
        passiveImbThreshold_lbw
        

        % internal storage
        snapshot
        
        % internal control parameters 
        limitClosePrice
        liveAmount           
        positionHoldingTime  
        OrderpendingTime     
        
        fillPrice
        trailingPrice  % running high for long position; running low for short position

        
        curStatus;     % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        

    end
    
    methods

        function this = tradesTrackerLite3_2(run_mode, tickSize, bidRatioThreshold, tradeImbThreshold, tradeImbThreshold2, passiveImbThreshold, passiveImbThreshold_lbw, shortSizeThreshold, trailingStopLevel)
            this.lbw = 8;
            
            this.run_mode = run_mode;
            this.tickSize = tickSize;
            this.bidRatioThreshold = bidRatioThreshold;
            this.tradeImbThreshold = tradeImbThreshold;
            this.tradeImbThreshold2 = tradeImbThreshold2;
            this.passiveImbThreshold = passiveImbThreshold;
            this.passiveImbThreshold_lbw = passiveImbThreshold_lbw;
            this.shortSizeThreshold = shortSizeThreshold;
            this.trailingStoplossLevel = trailingStopLevel;
            
            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.OrderpendingTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
            
            this.lbw_bid = nan(this.lbw, 1);
            this.lbw_ask = nan(this.lbw, 1);
            this.lbw_bidSize = nan(this.lbw, 1);
            this.lbw_askSize = nan(this.lbw, 1);
            %lbw_volume = nan(lbw,1);
            %lbw_vwap = nan(lbw,1);
            this.lbw_sizeBuy = nan(this.lbw, 1);
            this.lbw_sizeSell = nan(this.lbw, 1);
            
            
        end
        
        function OnInit( ~, ~, ~, ~ )
            
        end
        
        function update_lbwParams(this, snapshot, sizeBuy, sizeSell, passiveOrderBid, passiveOrderAsk )
            
            this.lbw_bid = [this.lbw_bid(2:end); snapshot.Bid(1)];
            this.lbw_ask = [this.lbw_ask(2:end); snapshot.Ask(1)];
            this.lbw_bidSize = [this.lbw_bidSize(2:end); snapshot.BidSize];
            this.lbw_askSize = [this.lbw_askSize(2:end); snapshot.AskSize];
            
            this.lbw_sizeBuy = [this.lbw_sizeBuy(2:end); sizeBuy];
            this.lbw_sizeSell = [this.lbw_sizeSell(2:end); sizeSell];
            
            this.lbw_PL_bid = [this.lbw_PL_bid(2:end); passiveOrderBid];
            this.lbw_PL_ask = [this.lbw_PL_ask(2:end); passiveOrderAsk];
            
        end
        
        function get_passiveLiquidity(this)
            
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            
            % end of trading session control 
            if (snapshot.Time.Hour == 00 && snapshot.Time.Minute == 59 && snapshot.Time.Second > 45) || (snapshot.Time.Hour == 14 && snapshot.Time.Minute == 59 && snapshot.Time.Second > 45)
                this.sessionEnd = true;
            else
                this.sessionEnd = false;
            end
            
            % strategy records 
            this.trades = [this.trades; nan];
            this.pnl    = [this.pnl   ; nan];
            this.snapshot = snapshot;
            
            % get aggressive and passive liquidity change
            sizeBuy = 0;
            sizeSell = 0;
            passiveOrderBid = 0;
            passiveOrderAsk = 0; 
            
            if ~isnan(this.lbw_ask(end)) && ~isnan(this.lbw_bid(end))
                if isnan(snapshot.VWAP)
                    snapshot.VWAP = 0;
                end
                [sizeBuy, sizeSell, ~, pxHigh, pxLow, qtyHigh, qtyLow] = ... 
                    tradeVolByVwap2(this.lbw_bid(end), this.lbw_ask(end), snapshot.VWAP, snapshot.Volume, this.tickSize);
               
                [ passiveOrderBid, passiveOrderAsk ] = ...
                passiveLiquidity( snapshot.Bid(1), snapshot.Ask(1), snapshot.BidSize, snapshot.AskSize, this.lbw_bid(end), this.lbw_ask(end), this.lbw_bidSize(end), this.lbw_askSize(end), pxHigh, pxLow, qtyHigh, qtyLow );
            

                %display([datestr(snapshot.Time), ' sizebuy: ' , num2str(sizeBuy), ' sizesell: ', num2str(sizeSell), ' PL_bid: ', num2str(passiveOrderBid), ' PL_ask: ', num2str(passiveOrderAsk)]);
            end
            
            % get passive liquidity change
            

            
            update_lbwParams(this, snapshot, sizeBuy, sizeSell, passiveOrderBid, passiveOrderAsk );


            
            %get signal 
            [signal, tradeImb, ~] = this.getSignal();
            this.signals = [this.signals; signal];
            switch this.curStatus
                case status.hasNothing              
                    if ready2trade(this)
                        % up signal                
                        if signal  == 1
                            if this.enterType == 0 % IOC in
                                if this.run_mode == mode.debug 
                                    display([datestr(snapshot.Time),' signal:', num2str(signal), '--- follow up trades, IOC IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true); 
                                
                            else        
                                if this.run_mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow up trades, limit IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                            end
                        % down signal 
                        elseif signal == -1
                            if this.enterType == 0 % IOC in
                                if this.run_mode == mode.debug
                                    display([datestr(snapshot.Time),' signal:', num2str(signal), '--- follow down trades, IOC IN! ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true);        
                                
                            else
                                if this.run_mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow down trades, limit IN ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                            end
                        end                      
                    end
                                         
                case status.hasOnePendingOrder
                    
                    this.OrderpendingTime =  this.OrderpendingTime + 1;
                    
                    if this.OrderpendingTime >= this.maxPendingPeriod 
                        if this.run_mode == mode.debug
                        display([datestr(this.snapshot.Time),' pending order max life reached ']);
                        end
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition

                    if this.sessionEnd
                        if this.run_mode == mode.debug
                            disp('session end, close out current positions!')
                        end
                        this.closePosition(InstrumentID, snapshot, true);
                        
                    else
                        % update trailing price
                        if this.liveAmount > 0 
                            this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.lbw_bid(end-1));
                        else
                            this.trailingPrice = this.trailingPrice - subplus(this.lbw_ask(end-1)-snapshot.Ask(1));
                        end

                        switch sign(signal*this.liveAmount)
                            case 1 % do nothing, keep the position

                            case -1 % ioc out followed by ioc in 

                                if this.liveAmount > 0
                                    if this.run_mode == mode.debug
                                        display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                    end
                                    this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                    % ioc in?
    %                                 if this.liveAmount == 0
    %                                 	if this.run_mode == mode.debug
    %                                         display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
    %                                     end
    %                                     this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
    %                                 end
                                elseif this.liveAmount < 0
                                    if this.run_mode == mode.debug
                                        display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                    end
                                    this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                    % ioc in?
    %                             	if this.liveAmount == 0
    %                                     if this.run_mode == mode.debug
    %                                         display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
    %                                     end
    %                                     this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
    %                                 end
                                end

                            case 0 % trailing pnl and exit signal determines limit out
                                % compute trailing pnl 
                                if this.liveAmount > 0
                                    trailingPnL = snapshot.Bid(1) - this.trailingPrice;
                                elseif this.liveAmount < 0
                                    trailingPnL = this.trailingPrice - snapshot.Ask(1);
                                end

                                if this.liveAmount > 0 
                                    if trailingPnL <= this.trailingStoplossLevel || tradeImb < - this.tradeImbThreshold2
                                        if this.run_mode == mode.debug
                                            display([datestr(snapshot.Time),' trailing stoploss: limit close long position']);
                                        end
                                        this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                                        this.limitClosePrice = snapshot.Ask(1);
                                    end
                                elseif this.liveAmount < 0 
                                    if trailingPnL <= this.trailingStoplossLevel || tradeImb > this.tradeImbThreshold2
                                        if this.run_mode == mode.debug
                                            display([datestr(snapshot.Time),' trailing stoploss: limit close short position']);
                                        end
                                        this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                                        this.limitClosePrice = snapshot.Bid(1);
                                    end
                                end
                        end
                    end
                    
                case status.hasOnePositionOnePendingOrder 
                    if this.sessionEnd
                        if this.run_mode == mode.debug
                            disp('session end, cancel all orders and close out current positions!')
                        end
                        this.CancelAllOrder();
                        this.closePosition(InstrumentID, snapshot, true);
                        
                    else
                        % update trailing price
                        if this.liveAmount > 0 
                            this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.lbw_bid(end-1));
                        else
                            this.trailingPrice = this.trailingPrice - subplus(this.lbw_ask(end-1)-snapshot.Ask(1));
                        end

                        switch sign(signal*this.liveAmount)
                            case 1 % cancel limit order, reset trailing price
                                if this.run_mode == mode.debug
                                    display([datestr(snapshot.Time),' cancel limit out order, reset trailing price! ']);
                                end
                                this.CancelAllOrder();
                                if this.liveAmount > 0
                                    this.trailingPrice = snapshot.Ask(1);
                                else
                                    this.trailingPrice = snapshot.Bid(1);
                                end
                            case -1 % cancel limit order, ioc out followed by ioc in 
                                this.CancelAllOrder();
                                if this.liveAmount > 0
                                    if this.run_mode == mode.debug
                                        display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                    end
                                    this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                    % ioc in
    %                                 if this.liveAmount == 0
    %                                 	if this.run_mode == mode.debug
    %                                         display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
    %                                     end
    %                                     this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
    %                                 end
                                elseif this.liveAmount < 0
                                    if this.run_mode == mode.debug
                                        display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                    end
                                    this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                    % ioc in 
    %                             	if this.liveAmount == 0
    %                                     if this.run_mode == mode.debug
    %                                         display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
    %                                     end
    %                                     this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
    %                                 end
                                end

                            case 0 % update limit price if necessary
                                if this.liveAmount > 0 

                                    if this.limitClosePrice ~= snapshot.Ask(1);
                                        if this.run_mode == mode.debug
                                        display([datestr(this.snapshot.Time),' update limit close long order to ', num2str(snapshot.Ask(1))]);
                                        end
                                        this.CancelAllOrder();

                                        this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);

                                        this.limitClosePrice = snapshot.Ask(1);
                                    end

                                elseif this.liveAmount < 0 

                                    if this.limitClosePrice ~= snapshot.Bid(1);
                                        if this.run_mode == mode.debug
                                         display([datestr(this.snapshot.Time),' update limit close short order to ', num2str(snapshot.Bid(1))]);
                                        end
                                        this.CancelAllOrder();

                                        this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);

                                        this.limitClosePrice = snapshot.Bid(1);
                                    end
                                end
                        end
                    end
            end

        end
            
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, ~ )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    %%%display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;
                    
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )

            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
                if isnan(this.trades(end))
                    this.trades(end) = Amount;
                else
                    this.trades(end) = this.trades(end) + Amount;
                end
            else
                this.liveAmount = this.liveAmount - Amount;
                if isnan(this.trades(end))
                    this.trades(end) = -Amount;
                else
                    this.trades(end) = this.trades(end) - Amount;
                end
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN order filled at price ',num2str(Order.Price)]);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.positionHoldingTime = 0;
                    
                    this.OrderpendingTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.run_mode == mode.debug
                        display([datestr(this.snapshot.Time),' IOC OUT success at price ',num2str(Order.Price) ,' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.run_mode == mode.debug
                        display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
        function closePosition(this, InstrumentID, snapshot, isIOC)
            
            if this.run_mode == mode.debug
                display([datestr(this.snapshot.Time),'close out current position ']);
            end
            
            if this.liveAmount > 0
                if isIOC
                    this.SellClose(InstrumentID, 1, snapshot.Bid(1), isIOC);
                else
                    this.SellClose(InstrumentID, 1, snapshot.Ask(1), isIOC);
                end
            elseif this.liveAmount < 0
                if isIOC
                    this.BuyClose(InstrumentID, 1, snapshot.Ask(1), isIOC);
                else
                    this.BuyClose(InstrumentID, 1, snapshot.Bid(1), isIOC);
                end
            end         
            
        end
        
        function [signal, tradeImb, sizeImb]= getSignal(this)

            
            
            tradeImb = this.lbw_sizeBuy(end) - this.lbw_sizeSell(end);
            
            sizeImb = this.snapshot.BidSize / (this.snapshot.BidSize + this.snapshot.AskSize);
            
%             if this.snapshot.BidSize < 3 || this.snapshot.AskSize < 3
%                 sizeImb = 0.5; % no signal in this case 
%             end
            
            if tradeImb > this.tradeImbThreshold ...
                && sizeImb > this.bidRatioThreshold ... % basic condition
                && this.snapshot.AskSize < this.shortSizeThreshold ...  % condition E
                && this.snapshot.Ask(1) - this.snapshot.Bid(1) == this.tickSize ...  % condition C
                && PL_signal(this, this.lbw_PL_ask(end), mean(this.lbw_PL_ask)) == 1 % condition B
                %&& ~psychologicalResistance(false, this.lbw_ask, this.lbw_askSize, 4500, this.lbw_sizeBuy, this.lbw_sizeSell, 50 )  % condtion A & D
                signal = 1;
            elseif tradeImb < -this.tradeImbThreshold ...
                    && sizeImb < (1-this.bidRatioThreshold) ...
                    && this.snapshot.BidSize < this.shortSizeThreshold ...
                    && this.snapshot.Ask(1) - this.snapshot.Bid(1) == this.tickSize ... 
                    && PL_signal(this, this.lbw_PL_ask(end), mean(this.lbw_PL_ask)) == -1
                    %&& ~psychologicalResistance( true, this.lbw_bid, this.lbw_bidSize, 4500, this.lbw_sizeBuy, this.lbw_sizeSell, 50 ) 
                signal = -1;    
            else    
                signal = 0;        
            end
                
        end
        
        function signal = PL_signal(this, passiveImb, passiveImb_lbw)
            
            %PL_total = this.lbw_PL_bid(end) - this.lbw_PL_ask(end);
            
%             if PL_total > PL_threshold
%                 signal = 1;
%             elseif PL_total < -PL_threshold
%                 signal = -1;
%             else
%                 signal = 0;
%             end

            if passiveImb > this.passiveImbThreshold && passiveImb_lbw > this.passiveImbThreshold_lbw
                signal = 1;
            elseif passiveImb < - this.passiveImbThreshold && passiveImb_lbw < -this.passiveImbThreshold_lbw
                signal = -1;
            else
                signal = 0;
            end
                
        end
        
        function ShowResults(this)    
            disp(['number of timeout IOC orders: ', num2str(this.numTimeOutIOCOrders)]);  
            disp(['number of pnl filter IOC orders: ', num2str(this.numPnLFilterIOCOrders)]);  
        end
       
        function ret = ready2trade(this)
            if this.sessionEnd | isnan(this.lbw_ask) 
                ret = false;
            else
                ret = true;           
            end
        end
    end
end