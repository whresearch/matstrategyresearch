classdef tradesTrackerLite2_6 < StrategyLite
    
    % lbw = 30 ; ioc in depends on weighted multiple indicators 
    % signal and trailing pnl determine wether ioc or pnl out
    % diff from tradesTrackerLite2: use bidrate to determine when to start
    % limit out
    properties
        % basic
        mode % run mode: analytic = 1, debug = 2, default = 0;
        tickSize = 10;
        
        % post-trade analysis
        numTimeOutIOCOrders   = 0;
        numPnLFilterIOCOrders = 0;
        
        % strategy parameters 
        lbw
        
        % vectors saved to generate signal 
        snapshot
        tradeImbVec;
        volumeVec;
        midVec;
        directionalOpinionVec;
        
        weightParams;
        
        features_mean_vec
        features_std_vec
        signalThreshold
        
        askSizes             
        bidSizes             
        
        tradeAmountUpVec            
        tradeAmountDownVec           
        tradeAmountUnknownVec
        
        preAsk               = nan;
        preBid               = nan;
        
        
        % 
        takeprofitLevel      = 240;
        stoplossLevel        = -70;
        trailingStoplossLevel = -30;

        % thresholds 
        tradeImbThreshold
        tradeImbThreshold2 = 40;
        sizeImbThreshold


        
        limitClosePrice
        
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        OrderpendingTime     
        
        fillPrice
        trailingPrice % running high for long position; running low for short position

        
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        
        cumPnl = 0;
        pnl    = [];
        trades = [];
        signals = [];
        signalVec = [];
    end
    
    methods

        function this = tradesTrackerLite2_6(run_mode, lbw, features_mean_vec, features_std_vec, weightParams, signalThreshold, trailingStoplossLevel)
            this.mode = run_mode;
            
            this.lbw = lbw;
            
            this.tradeImbVec = nan(this.lbw,1);
            this.volumeVec = nan(this.lbw,1);
            this.midVec = nan(this.lbw,1);
            this.directionalOpinionVec = nan(this.lbw,1);
            
            this.weightParams = weightParams;
            this.signalThreshold = signalThreshold;
            this.features_mean_vec = features_mean_vec;
            this.features_std_vec = features_std_vec;
            
            
            
            this.trailingStoplossLevel = trailingStoplossLevel;

            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.OrderpendingTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
       
            
            this.tradeAmountUpVec = zeros(this.lbw,1);  
            this.tradeAmountDownVec = zeros(this.lbw,1);  
            this.tradeAmountUnknownVec = zeros(this.lbw,1);  
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            if this.mode == mode.debug
                display([datestr(snapshot.Time),' ----------------------------- ']);
            end
            
            % update storage
            this.snapshot = snapshot;
            this.trades = [this.trades; nan];
            this.pnl    = [this.pnl   ; nan];
            
            % calculate trades directions  
            sizeBuy = 0;
            sizeSell = 0;
            sizeUnknown = 0;
            if ~isnan(this.preAsk) && ~isnan(this.preBid)
                if isnan(snapshot.VWAP)
                    snapshot.VWAP = 0;
                end
                [sizeBuy, sizeSell, sizeUnknown] = tradeVolByVwap2(this.preBid, this.preAsk, snapshot.VWAP, snapshot.Volume, this.tickSize);
            end
            
            % update storage
            this.tradeAmountUpVec = [this.tradeAmountUpVec(2:end);sizeBuy];
            this.tradeAmountDownVec = [this.tradeAmountDownVec(2:end);sizeSell];
            this.tradeAmountUnknownVec = [this.tradeAmountUpVec(2:end);sizeUnknown];
            
            % update tradeImbVec
            this.tradeImbVec = this.tradeAmountUpVec - this.tradeAmountDownVec;
            tradeImb = this.tradeImbVec(end);
            
            
            %update volumeVec
            this.volumeVec = [this.volumeVec(2:end); snapshot.Volume];
            
            %update midVec
            this.midVec = [this.midVec(2:end); 0.5*(snapshot.Ask(1)+snapshot.Bid(1))];
            
            %update directionalOpinionVec
            this.directionalOpinionVec = [this.directionalOpinionVec(2:end); tradeImb*2/snapshot.Volume;];
            if isnan(this.directionalOpinionVec(end))
                this.directionalOpinionVec(end) = 0;
            end
            [signal] = this.getSignal();
            this.signals = [this.signals;signal];
            
            if this.mode == mode.debug
                %display([datestr(snapshot.Time),' signal ----------------------------- ', num2str(signal)]);
            end
            switch this.curStatus
                case status.hasNothing              
                    if ready2trade(this)
                        % up signal                
                        if signal  == 1
                            if this.enterType == 0 % IOC in
                                if this.mode == mode.analytic || this.mode == mode.debug 
                                    display([datestr(snapshot.Time),' signal:', num2str(signal), ' follow up trades, IOC IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true); 
                                
                            else        
                                if this.mode == mode.analytic || this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow up trades, limit IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                            end
                        % down signal 
                        elseif signal == -1
                            if this.enterType == 0 % IOC in
                                if this.mode == mode.analytic || this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow down trades, IOC IN ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true);        
                                
                            else
                                if this.mode == mode.analytic || this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow down trades, limit IN ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                            end
                        end                      
                    end
                                         
                case status.hasOnePendingOrder
                    
                    this.OrderpendingTime =  this.OrderpendingTime + 1;
                    
                    if this.OrderpendingTime >= this.maxPendingPeriod 
                        if this.mode == mode.analytic || this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' pending order max life reached ']);
                        end
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition
                    % update trailing price
                    if this.liveAmount > 0 
                        this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.preBid);
                    else
                        this.trailingPrice = this.trailingPrice - subplus(this.preAsk-snapshot.Ask(1));
                    end
                    
                    
                    
                    switch sign(signal*this.liveAmount)
                        case 1 % do nothing, keep the position
                        
                        case -1 % ioc out followed by ioc in 
                            
                            if this.liveAmount > 0
                                if this.mode == mode.analytic || this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                end
                                this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                % ioc in?
%                                 if this.liveAmount == 0
%                                 	if this.mode == mode.analytic || this.mode == mode.debug
%                                         display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
%                                     end
%                                     this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
%                                 end
                            elseif this.liveAmount < 0
                                if this.mode == mode.analytic || this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                end
                                this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                % ioc in?
%                             	if this.liveAmount == 0
%                                     if this.mode == mode.analytic || this.mode == mode.debug
%                                         display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
%                                     end
%                                     this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
%                                 end
                            end
                            
                        case 0 % trailing pnl and exit signal determines limit out
                            % compute trailing pnl 
                            if this.liveAmount > 0
                                trailingPnL = snapshot.Bid(1) - this.trailingPrice;
                            elseif this.liveAmount < 0
                                trailingPnL = this.trailingPrice - snapshot.Ask(1);
                            end
                            
                            if this.liveAmount > 0 
                                if trailingPnL <= this.trailingStoplossLevel || signal == -1
                                    if this.mode == mode.analytic || this.mode == mode.debug
                                        display([datestr(snapshot.Time),' trailing stoploss: limit close long position']);
                                    end
                                    this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                                    this.limitClosePrice = snapshot.Ask(1);
                                end
                            elseif this.liveAmount < 0 
                                if trailingPnL <= this.trailingStoplossLevel || signal == 1
                                    if this.mode == mode.analytic || this.mode == mode.debug
                                        display([datestr(snapshot.Time),' trailing stoploss: limit close short position']);
                                    end
                                    this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                                    this.limitClosePrice = snapshot.Bid(1);
                                end
                            end
                    end
                    
                case status.hasOnePositionOnePendingOrder 
                    % update trailing price
                    if this.liveAmount > 0 
                        this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.preBid);
                    else
                        this.trailingPrice = this.trailingPrice - subplus(this.preAsk-snapshot.Ask(1));
                    end
                    
                    switch sign(signal*this.liveAmount)
                        case 1 % cancel limit order, reset trailing price
                            if this.mode == mode.analytic || this.mode == mode.debug
                                display([datestr(snapshot.Time),' cancel limit out order, reset trailing price! ']);
                            end
                            this.CancelAllOrder();
                            if this.liveAmount > 0
                                this.trailingPrice = snapshot.Ask(1);
                            else
                                this.trailingPrice = snapshot.Bid(1);
                            end
                        case -1 % cancel limit order, ioc out followed by ioc in 
                            this.CancelAllOrder();
                            if this.liveAmount > 0
                                if this.mode == mode.analytic || this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                end
                                this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                % ioc in
%                                 if this.liveAmount == 0
%                                 	if this.mode == mode.analytic || this.mode == mode.debug
%                                         display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
%                                     end
%                                     this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
%                                 end
                            elseif this.liveAmount < 0
                                if this.mode == mode.analytic || this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                end
                                this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                % ioc in 
%                             	if this.liveAmount == 0
%                                     if this.mode == mode.analytic || this.mode == mode.debug
%                                         display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
%                                     end
%                                     this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
%                                 end
                            end
                            
                        case 0 % update limit price if necessary
                            if this.liveAmount > 0 

                                if this.limitClosePrice ~= snapshot.Ask(1);
                                    if this.mode == mode.analytic || this.mode == mode.debug
                                    display([datestr(this.snapshot.Time),' update limit close long order to ', num2str(snapshot.Ask(1))]);
                                    end
                                    this.CancelAllOrder();

                                    this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);

                                    this.limitClosePrice = snapshot.Ask(1);
                                end

                            elseif this.liveAmount < 0 

                                if this.limitClosePrice ~= snapshot.Bid(1);
                                    if this.mode == mode.analytic || this.mode == mode.debug
                                     display([datestr(this.snapshot.Time),' update limit close short order to ', num2str(snapshot.Bid(1))]);
                                    end
                                    this.CancelAllOrder();

                                    this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);

                                    this.limitClosePrice = snapshot.Bid(1);
                                end

                            end
                    end
   
            end
            
            this.preAsk = snapshot.Ask;
            this.preBid = snapshot.Bid;
            
        end
            
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, ~ )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    %%%display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;     
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )

            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
                if isnan(this.trades(end))
                    this.trades(end) = Amount;
                else
                    this.trades(end) = this.trades(end) + Amount;
                end
            else
                this.liveAmount = this.liveAmount - Amount;
                if isnan(this.trades(end))
                    this.trades(end) = -Amount;
                else
                    this.trades(end) = this.trades(end) - Amount;
                end
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN order filled at price ',num2str(Order.Price)]);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.positionHoldingTime = 0;
                    
                    this.OrderpendingTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.mode == mode.analytic || this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' IOC OUT success at price ',num2str(Order.Price) ,' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.mode == mode.analytic || this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
        
        function [signal]= getSignal(this)

            filter = [0.1 0.2 0.3 0.4];
            if sum(isnan(this.tradeImbVec))>0 || sum(isnan(this.volumeVec))>0 || sum(isnan(this.midVec))>0
                signal = 0;
            else
                tradeImb = this.tradeImbVec(end);    % x1
                weightedTradeImb = filter*this.tradeImbVec(end-3:end);   % x2
                weightedDirectionOpinion = filter*this.directionalOpinionVec(end-3:end);   % x3   
                
                sizeImb = this.snapshot.BidSize/(this.snapshot.BidSize + this.snapshot.AskSize) - 0.5;  % x4
                [~, trendingMKT] = MKT2(this.midVec);                                           % x5        
                
                x1 = (tradeImb )/this.features_std_vec(1);
                x2 = (weightedTradeImb )/this.features_std_vec(2);
                x3 = (weightedDirectionOpinion )/this.features_std_vec(3);
                x4 = (sizeImb )/this.features_std_vec(4);
                x5 = (trendingMKT )/this.features_std_vec(5);

                x = [x1; x2; x3; x4; x5]; 

                %y = this.linearModel_paras * this.pc_matrix' * x;
                
                y = this.weightParams' * x;
                
                this.signalVec = [this.signalVec;y];
                
                if y > this.signalThreshold(1)
                    
                    signal = 1;
                elseif y < this.signalThreshold(2) 
                    signal = -1;    
                else    
                    signal = 0;         
                end
            
            end
        end
        
        function ShowResults(this)    
            disp(['number of timeout IOC orders: ', num2str(this.numTimeOutIOCOrders)]);  
            disp(['number of pnl filter IOC orders: ', num2str(this.numPnLFilterIOCOrders)]);  
        end
       
        function ret = ready2trade(this)
            ret = true;
%             if isnan(this.askSizes) || isnan(this.bidSizes)
%                 ret = false;
%             end              
        end
        
    end
    
end