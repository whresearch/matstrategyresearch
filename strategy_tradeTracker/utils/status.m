classdef(Enumeration) status < int32
    
 enumeration
  hasNothing(0)
  hasOnePendingOrder(1)
  hasOnePosition(2)
  hasOnePositionOnePendingOrder(3)
 end

 methods
 end
 
end

