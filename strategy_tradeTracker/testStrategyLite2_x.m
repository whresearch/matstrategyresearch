clc
clear
close all

tic

% train strategy parameters 
Data_paramsGen = LoadMat_SHFE_CU_Main(171);   % num 170 is 7 Jan 2016
confidenceLevel = 0.999;
[weightParams, feature_stds, up_threshold, down_threshold, signalVec] = f_tt2_x_paraGen(Data_paramsGen, confidenceLevel);

signalThresholds = [up_threshold  down_threshold]

% load back-test data
%Data = LoadMat_SHFE('shfe_20160107_cu1603.mat');
Data = LoadMat_SHFE_CU_Main(172);
Data.Volume(isnan(Data.Volume)) = 0;

dailyVol = sum(Data.Volume)
% Data.FilterByHours([21 0],[24 1]);

% create strategy
run_mode = mode.analytic;
lbw= 30;
x_mean_vec = [0 0 0 0 0];

trailingStoplossLevel = -20;
    
Strategy = tradesTrackerLite2_6(run_mode,lbw, x_mean_vec, feature_stds, weightParams, signalThresholds, trailingStoplossLevel);

 
 
 
% create backtest object and start back-testing
Btl = BacktestLite(Data, Strategy);

Btl.run();

% show results 
Btl.showResults();

Strategy.ShowResults();

trades = Strategy.trades;
pnl    = Strategy.pnl;
pnl_no_nan = pnl-2;
pnl_no_nan(isnan(pnl)) = 0;
cumPnL = cumsum(pnl_no_nan);


plotyy(1:length(Data.Last),Data.Last,1:length(Data.Last),cumPnL);


%tableView = f_tableView(Data);
tableView = f_tableViewTT5(Data, trades, pnl, cumPnL);

toc
