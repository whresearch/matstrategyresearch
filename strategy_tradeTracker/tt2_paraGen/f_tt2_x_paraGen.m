function [ weightParams, feature_stds, up_threshold, down_threshold, signalVec ] = f_tt5_genParams( data, confidenceLevel )
%generate parameters for strategy tradesTracker5, based on regression
%results
    %Data = data.GetRow(2:data.Length);
    Data = data;
    n = length(Data.Ask);

    law = 6;
    lbw = 30;

    % length of the regression vector is n-law-lbw+1 

    y = Data.Mid(law+lbw:end) - Data.Mid(lbw:end-law);

    % get buy and sell amount 
    bidPrice = Data.Bid;
    askPrice = Data.Ask;
    midPrice = 0.5*(bidPrice + askPrice);
    bidSize = Data.BidSize;
    askSize = Data.AskSize;
    VWAP = Data.VWAP;
    volume = Data.Volume;
    tickSize = 10;
    n = length(bidPrice);

    [~,~,~,~,sizeBuy,sizeSell] = fillRate(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize, 20);

    askRatio = askSize./(askSize + bidSize);
    bidRatio = bidSize./(askSize + bidSize);

    % prepare tradeImb
    weight_tradeImb = zeros(lbw,1);
    weight_tradeImb(1:4) = [0.4 0.3 0.2 0.1];
    tradeImb = sizeBuy - sizeSell;
    weightedTradeImb = filter(weight_tradeImb,1,tradeImb);
    weightedTradeImb = weightedTradeImb(lbw:end-law);

    weightedTradeImb(weightedTradeImb>100) = 100;
    weightedTradeImb(weightedTradeImb<-100) = -100;

    % prepare opinion direction
    directionOpinion = tradeImb*2./volume;
    directionOpinion(isnan(directionOpinion)) = 0;
    weighteddirectionOpinion = filter(weight_tradeImb,1,directionOpinion);
    weighteddirectionOpinion = weighteddirectionOpinion(lbw:end-law);


    % prepare sizeImb
    sizeImb = bidRatio - 0.5;
    sizeImb = sizeImb(lbw:end-law);


    % prepare mkt indicator
    trendingMKT = zeros(n-law-lbw+1,1);
    
    for i = 1:n-law-lbw
        [~ ,trendingMKT(i)] = MKT2(midPrice(i:i+lbw-1));
    end

    % do regression

    tradeImb = tradeImb(lbw:end-law);
    
    feature_stds = [std(tradeImb) std(weightedTradeImb) std(weighteddirectionOpinion) std(sizeImb) std(trendingMKT)];
    
    x1 = (tradeImb )/feature_stds(1);
    x2 = (weightedTradeImb )/feature_stds(2);
    x3 = (weighteddirectionOpinion )/feature_stds(3);
    x4 = (sizeImb )/feature_stds(4);
    x5 = (trendingMKT  )/feature_stds(5);

    
    
    x = [x1 x2 x3 x4 x5];
    
    [coeff,score,latent,tsquared,explained] = pca(x);

    pcax1 = x*coeff(:,1);
    pcax2 = x*coeff(:,2);
    pcax3 = x*coeff(:,3);
    pcax4 = x*coeff(:,4);

    xx = [ pcax1 pcax2 pcax3 pcax4];


    [b,bint,r,rint,stats]  = regress(y,xx);

    
    weightParams = coeff(:,1:4) * b;

    x = [x1 x2 x3 x4 x5]; 
    
    y =  x * weightParams;
    signalVec = y;
    
  
    up_threshold = quantile(y,confidenceLevel);
    down_threshold = quantile(y,1-confidenceLevel);
    
    
    numEvents = sum(y>up_threshold) + sum( y<down_threshold);
    
    numEvents
    
end

