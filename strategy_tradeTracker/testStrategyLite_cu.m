clc
clear
close all

tic

%dataNum = 6;

%Data = LoadMat_SHFE_CU_Main(dataNum);
Data = LoadMat_SHFE('shfe_20160317_cu1605.mat');

Data.Volume(isnan(Data.Volume)) = 0; 
volume = sum(Data.Volume);

%contact information 
disp([upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol), ' volume: ', num2str(volume), ', dataLength: ', num2str(length(Data.Ask))]);

% Data.FilterByHours([21 0],[24 1]);
run_mode = mode.debug;
tickSize = Data.Contract.TickSize;
bidRatioThreshold = 0.8;
tradeImbThreshold = 45;
tradeImbThreshold2 = 40;
passiveImbThreshold = 1;
passiveImbThreshold_lbw = 1;
shortSizeThreshold = 30;
trailingStopLevel = -2*tickSize;


% create strategy
Strategy = tradesTrackerLite3_2(run_mode, tickSize, bidRatioThreshold, tradeImbThreshold, tradeImbThreshold2, passiveImbThreshold, passiveImbThreshold_lbw, shortSizeThreshold, trailingStopLevel);

% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();

Btl.showResults();

Strategy.ShowResults();


trades = Strategy.trades;
pnl    = Strategy.pnl;
signal = Strategy.signals;
pnl_no_nan = pnl-0.2*tickSize;
pnl_no_nan(isnan(pnl)) = 0;
cumPnL = cumsum(pnl_no_nan);


plotyy(1:length(Data.Last),Data.Last,1:length(Data.Last),cumPnL);


%tableView = f_tableView(Data);
tableView = f_tableView(Data, trades, pnl, cumPnL, signal);

tradeTable = f_tradeTable(Data, trades, pnl, cumPnL, signal);

[feature_table, feature_mat]= f_featureTable( Data, trades, pnl );

%postTrade_analysis

toc
