classdef tradesTrackerLite1_1 < StrategyLite
    
    % lbw = 1 ; ioc in
    % after time filter 1 start limit out
    % after time filter 2 start ioc out
    % pnl filter exists but not used
    
    properties
        numTimeOutIOCOrders   = 0;
        numPnLFilterIOCOrders = 0;
        % strategy parameters 
        lbw                  
        
        askSizes             
        bidSizes             
        
        tradeAmountUp            
        tradeAmountDown           
        tradeAmountUnknown 
        preAsk               = nan;
        preBid               = nan;
        
        signalThreshold      = 70;
        
        takeprofitLevel      = 240;
        stoplossLevel        = -70;
        maxHoldingPeriod1   
        maxHoldingPeriod2    
        maxPendingPeriod     
        openRatioThreshold   
        askRatioThreshold    
        

        tradeImbThreshold;
        sizeImbThreshold;

        tickSize = 10;
        
        limitClosePrice
        
        % storage
        values
        snapshot
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        OrderpendingTime     
        
        fillPrice;
        
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        cumPnl = 0;
        pnl    = [];
        trades = [];
        
    end
    
    methods

        function this = tradesTrackerLite1_1(sizeImbThreshold, tradeImbThreshold, HoldingPeriod1, HoldingPeriod2)
            this.sizeImbThreshold = sizeImbThreshold;
            this.tradeImbThreshold = tradeImbThreshold;
            this.maxHoldingPeriod1 = HoldingPeriod1;
            this.maxHoldingPeriod2 = HoldingPeriod1 + HoldingPeriod2;


            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.OrderpendingTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
       
            this.values = NaN(this.lbw, 1);
            
            this.lbw = 30;
            this.tradeAmountUp = zeros(this.lbw,1);  
            this.tradeAmountDown = zeros(this.lbw,1);  
            this.tradeAmountUnknown = zeros(this.lbw,1);  
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            this.trades = [this.trades; nan];
            this.pnl    = [this.pnl   ; nan];
            this.snapshot = snapshot;
            sizeBuy = 0;
            sizeSell = 0;
            sizeUnknown = 0;
            
            if ~isnan(this.preAsk) && ~isnan(this.preBid)
                if isnan(snapshot.VWAP)
                    snapshot.VWAP = 0;
                end
                [sizeBuy, sizeSell, sizeUnknown] = tradeVolByVwap2(this.preBid, this.preAsk, snapshot.VWAP, snapshot.Volume, this.tickSize);
               
                %display([datestr(snapshot.Time),' sizebuy: ', num2str(sizeBuy),' sizesell: ', num2str(sizeSell),' sizeunknown: ',num2str(sizeUnknown)]);
            end
            
            this.preAsk = snapshot.Ask;
            this.preBid = snapshot.Bid;
            
            this.tradeAmountUp = [this.tradeAmountUp(2:end);sizeBuy];
            this.tradeAmountDown = [this.tradeAmountDown(2:end);sizeSell];
            this.tradeAmountUnknown = [this.tradeAmountUp(2:end);sizeUnknown];
            
            signal = this.getSignal();
      
            switch this.curStatus
                case status.hasNothing              
                    if ready2trade(this)
                        % up signal                
                        if signal  == 1
                            if this.enterType == 0 % IOC in
                                display([datestr(snapshot.Time),' signal:', num2str(signal), ' follow up trades, IOC IN! ']);
                                this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true); 
                                
                            else        
                                display([datestr(snapshot.Time),' follow up trades, limit IN! ']);
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                            end
                        % down signal 
                        elseif signal == -1
                            if this.enterType == 0 % IOC in
                                display([datestr(snapshot.Time),' follow down trades, IOC IN ']);
                                this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true);        
                                
                            else
                                display([datestr(snapshot.Time),' follow down trades, limit IN ']);
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                            end
                        end                      
                    end
                                         
                case status.hasOnePendingOrder
                    
                    this.OrderpendingTime =  this.OrderpendingTime + 1;
                    
                    if this.OrderpendingTime >= this.maxPendingPeriod 
                        
                        display([datestr(snapshot.Time),' pending order max life reached ']);
                        
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                        %disp(['position holding time is ',num2str(this.positionHoldingTime)]);
                    
                        % pnl filter 
                    if this.liveAmount > 0
                        curOrderPnL = snapshot.Bid(1) - this.fillPrice;
                    elseif this.liveAmount < 0
                        curOrderPnL = this.fillPrice - snapshot.Ask(1);
                    end
                    
                    
                    if this.liveAmount > 0 && curOrderPnL >= this.takeprofitLevel
                        
                            display([datestr(this.snapshot.Time),' take-profit: IOC close long position']);
                            this.numPnLFilterIOCOrders = this.numPnLFilterIOCOrders + 1;
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                            
                    elseif this.liveAmount < 0 && curOrderPnL >= this.takeprofitLevel
                            
                            display([datestr(this.snapshot.Time),' take-profit: IOC close short position']);
                            this.numPnLFilterIOCOrders = this.numPnLFilterIOCOrders + 1;
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    elseif this.liveAmount > 0 && curOrderPnL <= this.stoplossLevel
                        
                            display([datestr(this.snapshot.Time),' stop-loss: IOC close long position']);
                            this.numPnLFilterIOCOrders = this.numPnLFilterIOCOrders + 1;
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && curOrderPnL <= this.stoplossLevel
                            
                            display([datestr(this.snapshot.Time),' stop-loss: IOC close short position']);
                            this.numPnLFilterIOCOrders = this.numPnLFilterIOCOrders + 1;
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    
                    % time filter 
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(snapshot.Time),' time filter 1: limit close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                            this.limitClosePrice = snapshot.Ask(1);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(snapshot.Time),' time filter 1: limit close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                            this.limitClosePrice = snapshot.Bid(1);
                            
                        end
                        
                        this.curStatus = 3;
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            this.numTimeOutIOCOrders = this.numTimeOutIOCOrders + 1;
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            this.numTimeOutIOCOrders = this.numTimeOutIOCOrders + 1;
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    

                       
                    
                case status.hasOnePositionOnePendingOrder 
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                   
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                            
                            if this.limitClosePrice ~= snapshot.Ask(1);
                                
                                display([datestr(this.snapshot.Time),' update limit close long order! ']);
                                
                                this.CancelAllOrder();

                                this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                                this.limitClosePrice = snapshot.Ask(1);
                            end
                            
                        elseif this.liveAmount < 0 
                            
                            if this.limitClosePrice ~= snapshot.Bid(1);
                                
                                display([datestr(this.snapshot.Time),'  update limit close short order! ']);
                                
                                this.CancelAllOrder();
                                
                                this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                                this.limitClosePrice = snapshot.Bid(1);
                            end
                            
                        end
                        
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            this.numTimeOutIOCOrders = this.numTimeOutIOCOrders + 1;
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            this.numTimeOutIOCOrders = this.numTimeOutIOCOrders + 1;
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    
            end
            
        end
            
       

        
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, ~ )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    %%%display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;
                    
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )
            
            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
                if isnan(this.trades(end))
                    this.trades(end) = Amount;
                else
                    this.trades(end) = this.trades(end) + Amount;
                end
            else
                this.liveAmount = this.liveAmount - Amount;
                if isnan(this.trades(end))
                    this.trades(end) = -Amount;
                else
                    this.trades(end) = this.trades(end) - Amount;
                end
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN order filled at price ',num2str(Order.Price)]);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.OrderpendingTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    
                    display([datestr(this.snapshot.Time),' IOC OUT success at price ',num2str(Order.Price) ,' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    
                    display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
        
        function [signal, tradeImb, sizeImb]= getSignal(this)
            
            tradeImb = this.tradeAmountUp(end) - this.tradeAmountDown(end);
            
            sizeImb = this.snapshot.BidSize / (this.snapshot.BidSize + this.snapshot.AskSize);
            
%             if this.snapshot.BidSize < 3 || this.snapshot.AskSize < 3
%                 sizeImb = 0.5; % no signal in this case 
%             end
            
            if tradeImb > this.tradeImbThreshold && sizeImb > this.sizeImbThreshold
                signal = 1;
            elseif tradeImb < -this.tradeImbThreshold && sizeImb < (1-this.sizeImbThreshold)
                signal = -1;    
            else    
                signal = 0;        
            end
                
        end
        
        function ShowResults(this)    
            disp(['number of timeout IOC orders: ', num2str(this.numTimeOutIOCOrders)]);  
            disp(['number of pnl filter IOC orders: ', num2str(this.numPnLFilterIOCOrders)]);  
        end
       
        function ret = ready2trade(this)
            ret = true;
%             if isnan(this.askSizes) || isnan(this.bidSizes)
%                 ret = false;
%             end              
        end
        
    end
    
end