clear
clc
load('featureMat_day1_to_day24.mat');

disp(' lbw8_pl, pl, lbw_priceChange, bidRatio, tradeImb, largeSpread, shortSize, trades, pnl')


pnl = featuresPnL(:,9);
trade = featuresPnL(:,8);
up_idx = trade == 1;
down_idx = trade == -1;
disp(['mean pnl: ', num2str(mean(pnl))]);
disp(['mean pnl up signal: ', num2str(mean(pnl(up_idx)))]);
disp(['mean pnl down signal: ', num2str(mean(pnl(down_idx)))]);
disp('...................................................................................................')

% shortSize
shortSize = featuresPnL(:,7);
shortSize_threshold = quantile(shortSize, 0.8);
shortSize_threshold = 30;
shortSize_idx = shortSize < shortSize_threshold;
disp(['mean pnl considering feature shortSize: ', num2str(mean(pnl(shortSize_idx)))]);
disp(['mean pnl considering feature shortSize up: ', num2str(mean(pnl(shortSize_idx & up_idx)))]);
disp(['mean pnl considering feature shortSize down: ', num2str(mean(pnl(shortSize_idx & down_idx)))]);
disp('...................................................................................................')

n = 40;
mean_pnl = zeros(n,1);
for shortSize_threshold = 1:n
    shortSize_idx = shortSize < shortSize_threshold;
    mean_pnl(i) = mean(pnl(shortSize_idx));
    
end


% largesPread
largeSpread =  featuresPnL(:,6);
largeSpread_idx = largeSpread == 0;
%mean(pnl(largeSpread_idx))
disp(['mean pnl considering feature largeSpread: ', num2str(mean(pnl(largeSpread_idx)))]);
disp(['mean pnl considering feature largeSpread up: ', num2str(mean(pnl(largeSpread_idx & up_idx)))]);
disp(['mean pnl considering feature largeSpread down: ', num2str(mean(pnl(largeSpread_idx & down_idx)))]);
disp('...................................................................................................')

% joint largesPread & shortSize
%mean(pnl(largeSpread_idx & shortSize_idx))
disp(['mean pnl considering feature shortSize & largeSpread : ', num2str(mean(pnl(largeSpread_idx & shortSize_idx)))]);
disp(['mean pnl considering feature shortSize & largeSpread up: ', num2str(mean(pnl(largeSpread_idx & shortSize_idx & up_idx)))]);
disp(['mean pnl considering feature shortSize & largeSpread down: ', num2str(mean(pnl(largeSpread_idx & shortSize_idx & down_idx)))]);
disp('...................................................................................................')

% passive liquidity
lbw8_pl = featuresPnL(:,1);
pl = featuresPnL(:,2);
%scatter(pl(up_idx),pnl(up_idx))
quantileVec = 0.1 : 0.1 : 0.9;

quantileValues_up = quantile(lbw8_pl(up_idx),quantileVec);
quantileValues_up = [-inf quantileValues_up inf];

idx = cell(10,1);

for i = 1:10
    
    idx{i} = lbw8_pl > quantileValues_up(i) & lbw8_pl <= quantileValues_up(i+1);
    
    disp(['mean pnl considering feature pl up: ', num2str(mean(pnl(idx{i} & up_idx)))]);
    
end    

% conclusion: pl > 2 , optimisation on 0 , 1 , 2 needed 
% conclusion: lbw8_pl > -0.875 















