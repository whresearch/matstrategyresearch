clc
clear

tic

numDays = 24;

pnls = zeros(numDays,1);

volumes = zeros(numDays,1);

numTrades = zeros(numDays,1);

tradeImbThreshold = 40;
sizeImbThreshold = 0.85;

disp(['tradeImbThreshold: ', num2str(tradeImbThreshold), ', sizeImbThreshold: ', num2str(sizeImbThreshold)]);

feature_mat_all = [];

parallel_computing = 0;

if parallel_computing
    parfor i = 1:numDays

        Data = LoadMat_SHFE_CU_Main(i); 
        volume = sum(Data.Volume);
        [ pnl, nTrades, feature_mat ]= f_testStrategy_CU( Data, tradeImbThreshold, sizeImbThreshold );
        disp(['Day ', num2str(i), ' ', upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol), ' --- volume: ', num2str(volume), ', number of trades: ', num2str(nTrades) ', pnl: ', num2str(pnl)]);
        pnls(i) = pnl;
        numTrades(i) = nTrades;


        
        
        
        
        feature_mat_all = [feature_mat_all; feature_mat];

    end
    delete(gcp)
else
    for i = 1:numDays

        Data = LoadMat_SHFE_CU_Main(i); 
        volume = sum(Data.Volume);
        [ pnl, nTrades, feature_mat ]= f_testStrategy_CU( Data, tradeImbThreshold, sizeImbThreshold );
        disp(['Day ', num2str(i), ' ', upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol), ' --- volume: ', num2str(volume), ', number of trades: ', num2str(nTrades) ', pnl: ', num2str(pnl)]);
        pnls(i) = pnl;
        numTrades(i) = nTrades;
        
        pnlVec = feature_mat(:,9);
        tradeVec = feature_mat(:,8);
        up_idx = tradeVec == 1;
        down_idx = tradeVec == -1;
        disp(['mean pnl up signal: ', num2str(mean(pnlVec(up_idx)))]);
        disp(['mean pnl down signal: ', num2str(mean(pnlVec(down_idx)))]);
        feature_mat_all = [feature_mat_all; feature_mat];

    end
    
end

sharpe_ratio = mean(pnls)/std(pnls);

disp(['sharpe ratio: ', num2str(sharpe_ratio)]);
disp('mat columns: lbw8_pn, pl, lbw_priceChange, bidRatio, tradeImb, largeSpread, shortSize, trades, pnl')
disp('..............................................................................................')

toc