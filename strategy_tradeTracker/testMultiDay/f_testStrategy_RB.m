function [ pnl, numTrades ] = f_testStrategy_RB( Data, tradeImbThreshold, sizeImbThreshold )

    if nargin < 3
        tradeImbThreshold = 750;
        sizeImbThreshold = 0.88;
    end
        run_mode = mode.default;
    tickSize = Data.Contract.TickSize;
    trailingStoplossLevel = -2 * tickSize;
%     shortSizeThreshold = 1000;
%     
%     Strategy = tradesTrackerLite3ce_2(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, shortSizeThreshold, trailingStoplossLevel);

    Strategy = tradesTrackerLite1_4(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, trailingStoplossLevel);

    Btl = BacktestLite(Data, Strategy);

    Btl.run();

    pnl = Btl.pnl;
    
    pnls = Strategy.pnl;

    pnls(isnan(pnls)) = [];
    
    numTrades = length(pnls);
    
    
end

