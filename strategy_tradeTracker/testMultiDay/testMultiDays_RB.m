clc
clear

tic

numDays = 24;

pnls = zeros(numDays,1);

volumes = zeros(numDays,1);

numTrades = zeros(numDays,1);

tradeImbThreshold = 650;
sizeImbThreshold = 0.85;
disp(['tradeImbThreshold: ', num2str(tradeImbThreshold), ', sizeImbThreshold: ', num2str(sizeImbThreshold)]);

parfor i = 1:numDays
    
    Data = LoadMat_SHFE_RB_Main(i); 
    volume = sum(Data.Volume);
    [ pnl, nTrades ]= f_testStrategy_RB(Data, tradeImbThreshold, sizeImbThreshold);
    disp(['Day ', num2str(i), ' ', upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol), ' --- volume: ', num2str(volume), ', number of trades: ', num2str(nTrades) ', pnl: ', num2str(pnl)]);
    pnls(i) = pnl;
    numTrades(i) = nTrades;

end

sharpe_ratio = mean(pnls)/std(pnls);

disp(['sharpe ratio: ', num2str(sharpe_ratio)]);

disp('...............................................................................................................')

toc

delete(gcp)