clc
clear
close all

tic

dataNum = 4;

% Data = LoadMat_SHFE('shfe_20160201_rb1605.mat');
Data = LoadMat_SHFE_RB_Main(dataNum); 

Data.Volume(isnan(Data.Volume)) = 0; 
volume = sum(Data.Volume);

%contact information 
disp([upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol), ' volume: ', num2str(volume), ', dataLength: ', num2str(length(Data.Ask))]);


% Data.FilterByHours([21 0],[24 1]);

tradeImbThreshold = 650;
sizeImbThreshold = 0.85;
shortSizeThreshold = 1000;
% create strategy
tickSize = Data.Contract.TickSize;
trailingStoplossLevel = -2 * tickSize;
pl_threshold = tradeImbThreshold*(-100);
run_mode = mode.debug;

%Strategy = tradesTrackerLite3abcde_2(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, shortSizeThreshold, trailingStoplossLevel, pl_threshold);
 
Strategy = tradesTrackerLite1_4(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, trailingStoplossLevel);

 
 
 
% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();

Btl.showResults();

Strategy.ShowResults();


trades = Strategy.trades;
pnl    = Strategy.pnl;
signal = Strategy.signals;
pnl_no_nan = pnl-0.2;
pnl_no_nan(isnan(pnl)) = 0;
cumPnL = cumsum(pnl_no_nan);


plotyy(1:length(Data.Last),Data.Last,1:length(Data.Last),cumPnL);


%tableView = f_tableView(Data);
tableView = f_tableView(Data, trades, pnl, cumPnL, signal);

tradeTable = f_tradeTable(Data, trades, pnl, cumPnL, signal);

postTrade_analysis

toc







