clc
clear
close all

tic

% Data = LoadMat_SHFE_CU_Main(135);

%Data = LoadMat_SHFE('shfe_20160122_cu1603.mat');
Data = LoadMat_SHFE('shfe_20160122_ni1605.mat');
Data.Volume(isnan(Data.Volume)) = 0; 
dailyVol = sum(Data.Volume)
% Data.FilterByHours([21 0],[24 1]);

run_mode = mode.debug;

tradeImbThreshold = 40;
sizeImbThreshold = 0.85; % bid/ask ratio
%shortSizeThreshold = 1000;
% create strategy
tickSize = Data.Contract.TickSize;
trailingStoplossLevel = -2 * tickSize;
pl_threshold = 10;


Strategy = tradesTrackerLite1_4(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, trailingStoplossLevel);

%Strategy = tradesTrackerLite3abcde_2(run_mode, tickSize, sizeImbThreshold, tradeImbThreshold, shortSizeThreshold, trailingStoplossLevel, pl_threshold);

 
 
 
% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();

Btl.showResults();

Strategy.ShowResults();


trades = Strategy.trades;
pnl    = Strategy.pnl;
signal = Strategy.signals;
pnl_no_nan = pnl-0.2;
pnl_no_nan(isnan(pnl)) = 0;
cumPnL = cumsum(pnl_no_nan);


plotyy(1:length(Data.Last),Data.Last,1:length(Data.Last),cumPnL);


%tableView = f_tableView(Data);
tableView = f_tableView(Data, trades, pnl, cumPnL, signal);

tradeTable = f_tradeTable(Data, trades, pnl, cumPnL, signal);

toc
