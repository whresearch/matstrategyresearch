classdef tradesTrackerLite1_3 < StrategyLite
    
    % lbw = 1 ; ioc in
    % signal and trailing pnl determine wether ioc or pnl out
    % diff from tradesTrackerLite2: use bidrate to determine when to start
    % limit out
    properties
        mode 
        numTimeOutIOCOrders   = 0;
        numPnLFilterIOCOrders = 0;
        % strategy parameters 
        lbw                  
        
        askSizes             
        bidSizes             
        
        tradeAmountUp            
        tradeAmountDown           
        tradeAmountUnknown 
        preAsk               = nan;
        preBid               = nan;
        
        signalThreshold      = 70;
        
        takeprofitLevel      = 240;
        stoplossLevel        = -70;
        trailingStoplossLevel = -30;

        tradeImbThreshold
        sizeImbThreshold

        tickSize = 10;
        
        limitClosePrice
        
        % storage
        values
        snapshot
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        OrderpendingTime     
        
        fillPrice
        trailingPrice % running high for long position; running low for short position

        
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        
        cumPnl = 0;
        pnl    = [];
        trades = [];
        
    end
    
    methods

        function this = tradesTrackerLite1_3(run_mode, sizeImbThreshold, tradeImbThreshold, trailingStoplossLevel)
            this.mode = run_mode;
            this.sizeImbThreshold = sizeImbThreshold;
            this.tradeImbThreshold = tradeImbThreshold;

            this.trailingStoplossLevel = trailingStoplossLevel;

            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.OrderpendingTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
       
            this.values = NaN(this.lbw, 1);
            
            this.lbw = 30;
            this.tradeAmountUp = zeros(this.lbw,1);  
            this.tradeAmountDown = zeros(this.lbw,1);  
            this.tradeAmountUnknown = zeros(this.lbw,1);  
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            this.trades = [this.trades; nan];
            this.pnl    = [this.pnl   ; nan];
            this.snapshot = snapshot;
            sizeBuy = 0;
            sizeSell = 0;
            sizeUnknown = 0;
             
            if ~isnan(this.preAsk) && ~isnan(this.preBid)
                if isnan(snapshot.VWAP)
                    snapshot.VWAP = 0;
                end
                [sizeBuy, sizeSell, sizeUnknown] = tradeVolByVwap2(this.preBid, this.preAsk, snapshot.VWAP, snapshot.Volume, this.tickSize);
               
                %display([datestr(snapshot.Time),' sizebuy: ', num2str(sizeBuy),' sizesell: ', num2str(sizeSell),' sizeunknown: ',num2str(sizeUnknown)]);
            end
            
            
            this.tradeAmountUp = [this.tradeAmountUp(2:end);sizeBuy];
            this.tradeAmountDown = [this.tradeAmountDown(2:end);sizeSell];
            this.tradeAmountUnknown = [this.tradeAmountUp(2:end);sizeUnknown];
            
            [signal, ~, sizeImb] = this.getSignal();
      
            switch this.curStatus
                case status.hasNothing              
                    if ready2trade(this)
                        % up signal                
                        if signal  == 1
                            if this.enterType == 0 % IOC in
                                if this.mode == mode.debug 
                                    display([datestr(snapshot.Time),' signal:', num2str(signal), ' follow up trades, IOC IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true); 
                                
                            else        
                                if this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow up trades, limit IN! ']);
                                end
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                            end
                        % down signal 
                        elseif signal == -1
                            if this.enterType == 0 % IOC in
                                if this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow down trades, IOC IN ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true);        
                                
                            else
                                if this.mode == mode.debug
                                display([datestr(this.snapshot.Time),' follow down trades, limit IN ']);
                                end
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                            end
                        end                      
                    end
                                         
                case status.hasOnePendingOrder
                    
                    this.OrderpendingTime =  this.OrderpendingTime + 1;
                    
                    if this.OrderpendingTime >= this.maxPendingPeriod 
                        if this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' pending order max life reached ']);
                        end
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition
                    % update trailing price
                    if this.liveAmount > 0 
                        this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.preBid);
                    else
                        this.trailingPrice = this.trailingPrice - subplus(this.preAsk-snapshot.Ask(1));
                    end
                    
                    
                    
                    switch sign(signal*this.liveAmount)
                        case 1 % do nothing, keep the position
                        
                        case -1 % ioc out followed by ioc in 
                            
                            if this.liveAmount > 0
                                if this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                end
                                this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                % ioc in?
                                if this.liveAmount == 0
                                	if this.mode == mode.debug
                                        display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
                                    end
                                    this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
                                end
                            elseif this.liveAmount < 0
                                if this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                end
                                this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                % ioc in?
                            	if this.liveAmount == 0
                                    if this.mode == mode.debug
                                        display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
                                    end
                                    this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
                                end
                            end
                            
                        case 0 % trailing pnl determines limit out
                            % compute trailing pnl 
                            if this.liveAmount > 0
                                trailingPnL = snapshot.Bid(1) - this.trailingPrice;
                            elseif this.liveAmount < 0
                                trailingPnL = this.trailingPrice - snapshot.Ask(1);
                            end
                            
                            if this.liveAmount > 0 
                                if trailingPnL <= this.trailingStoplossLevel || sizeImb < (1-this.sizeImbThreshold)
                                    if this.mode == mode.debug
                                        display([datestr(snapshot.Time),' trailing stoploss: limit close long position']);
                                    end
                                    this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                                    this.limitClosePrice = snapshot.Ask(1);
                                end
                            elseif this.liveAmount < 0 
                                if trailingPnL <= this.trailingStoplossLevel || sizeImb > this.sizeImbThreshold
                                    if this.mode == mode.debug
                                        display([datestr(snapshot.Time),' trailing stoploss: limit close short position']);
                                    end
                                    this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                                    this.limitClosePrice = snapshot.Bid(1);
                                end
                            end
                    end
                    
                case status.hasOnePositionOnePendingOrder 
                    % update trailing price
                    if this.liveAmount > 0 
                        this.trailingPrice = this.trailingPrice + subplus(snapshot.Bid(1)-this.preBid);
                    else
                        this.trailingPrice = this.trailingPrice - subplus(this.preAsk-snapshot.Ask(1));
                    end
                    
                    switch sign(signal*this.liveAmount)
                        case 1 % cancel limit order, reset trailing price
                            if this.mode == mode.debug
                                display([datestr(snapshot.Time),' cancel limit out order, reset trailing price! ']);
                            end
                            this.CancelAllOrder();
                            if this.liveAmount > 0
                                this.trailingPrice = snapshot.Ask(1);
                            else
                                this.trailingPrice = snapshot.Bid(1);
                            end
                        case -1 % cancel limit order, ioc out followed by ioc in 
                            this.CancelAllOrder();
                            if this.liveAmount > 0
                                if this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close long position']);
                                end
                                this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                                % ioc in
                                if this.liveAmount == 0
                                	if this.mode == mode.debug
                                        display([datestr(snapshot.Time), ' follow down trades, IOC IN ']);
                                    end
                                    this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
                                end
                            elseif this.liveAmount < 0
                                if this.mode == mode.debug
                                    display([datestr(snapshot.Time), ' signal reverses: IOC close short position']);
                                end
                                this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                                % ioc in 
                            	if this.liveAmount == 0
                                    if this.mode == mode.debug
                                        display([datestr(snapshot.Time), ' follow up trades, IOC IN! ']);
                                    end
                                    this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);  
                                end
                            end
                            
                        case 0 % update limit price if necessary
                            if this.liveAmount > 0 

                                if this.limitClosePrice ~= snapshot.Ask(1);
                                    if this.mode == mode.debug
                                    display([datestr(this.snapshot.Time),' update limit close long order to ', num2str(snapshot.Ask(1))]);
                                    end
                                    this.CancelAllOrder();

                                    this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);

                                    this.limitClosePrice = snapshot.Ask(1);
                                end

                            elseif this.liveAmount < 0 

                                if this.limitClosePrice ~= snapshot.Bid(1);
                                    if this.mode == mode.debug
                                     display([datestr(this.snapshot.Time),' update limit close short order to ', num2str(snapshot.Bid(1))]);
                                    end
                                    this.CancelAllOrder();

                                    this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);

                                    this.limitClosePrice = snapshot.Bid(1);
                                end

                            end
                    end
   
            end
            
            this.preAsk = snapshot.Ask;
            this.preBid = snapshot.Bid;
            
        end
            
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.OrderpendingTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, ~ )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    %%%display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;
                    
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )

            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
                if isnan(this.trades(end))
                    this.trades(end) = Amount;
                else
                    this.trades(end) = this.trades(end) + Amount;
                end
            else
                this.liveAmount = this.liveAmount - Amount;
                if isnan(this.trades(end))
                    this.trades(end) = -Amount;
                else
                    this.trades(end) = this.trades(end) - Amount;
                end
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN order filled at price ',num2str(Order.Price)]);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.trailingPrice = this.fillPrice;
                    
                    this.positionHoldingTime = 0;
                    
                    this.OrderpendingTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' IOC OUT success at price ',num2str(Order.Price) ,' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        curOrderPnL = this.fillPrice - Price; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    else
                        curOrderPnL = Price - this.fillPrice; 
                        this.cumPnl = this.cumPnl + curOrderPnL;
                        this.pnl(end) = curOrderPnL;
                    end
                    if this.mode == mode.debug
                        display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(curOrderPnL),'/',num2str(this.cumPnl)]);
                    end
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
        
        function [signal, tradeImb, sizeImb]= getSignal(this)
            
            tradeImb = this.tradeAmountUp(end) - this.tradeAmountDown(end);
            
            sizeImb = this.snapshot.BidSize / (this.snapshot.BidSize + this.snapshot.AskSize);
            
%             if this.snapshot.BidSize < 3 || this.snapshot.AskSize < 3
%                 sizeImb = 0.5; % no signal in this case 
%             end
            
            if tradeImb > this.tradeImbThreshold && sizeImb > this.sizeImbThreshold
                signal = 1;
            elseif tradeImb < -this.tradeImbThreshold && sizeImb < (1-this.sizeImbThreshold)
                signal = -1;    
            else    
                signal = 0;        
            end
                
        end
        
        function ShowResults(this)    
            disp(['number of timeout IOC orders: ', num2str(this.numTimeOutIOCOrders)]);  
            disp(['number of pnl filter IOC orders: ', num2str(this.numPnLFilterIOCOrders)]);  
        end
       
        function ret = ready2trade(this)
            ret = true;
%             if isnan(this.askSizes) || isnan(this.bidSizes)
%                 ret = false;
%             end              
        end
        
    end
    
end