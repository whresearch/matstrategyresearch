function [ score ] = tradesTrackerLiteScore( Data, Params )
%TRADESTRACKERLITESCORE Summary of this function goes here
%   Detailed explanation goes here

Strategy = tradesTrackerLite(double(Params(1))/100, double(Params(2)), double(Params(3)), double(Params(4)));

%disp (['param ' num2str(double(Params(1))/100) ' ' num2str(Params(2)) ' ' num2str(Params(3)) ' ' num2str(Params(4))]);
% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();
Btl.showResults();
disp (['param ' num2str(double(Params(1))/100) ' ' num2str(Params(2)) ' ' num2str(Params(3)) ' ' num2str(Params(4))]);

score = -Btl.sharpe;

%disp (['Sharpe:' num2str(score)]);
disp ('----------------------------------------------------------------------------------------')
end

