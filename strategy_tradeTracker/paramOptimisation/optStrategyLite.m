clc
clear
close all

%profile on
% load data
% StartDate = datetime(2015, 9, 10);
% EndDate = datetime(2015, 9, 10);
% Data = LoadMat_SHFE_Range('cu1511', StartDate, EndDate);

Data = LoadMat_SHFE_CU_Main(126);

FitnessFun2 = @(params)tradesTrackerLiteScore (Data, params);

Options = gaoptimset('UseParallel', false, 'StallGenLimit', 5, 'Display', 'iter'); 



[x,fval,exitflag,output,population,scores]  = ga(FitnessFun2, 4, [], [], [], [], [60 20 5 1], [90 100 50 50], [], [1 2 3 4], Options);



