classdef intradayRevLite < StrategyLite
    
    properties

        % strategy parameters 
        lbw                  = 30;
        mktThreshold         = 0.51;
        takeprofitLevel      = 60;
        stoplossLevel        = -40;
        maxHoldingPeriod1    = 200;
        maxHoldingPeriod2    = 300;
        maxPendingPeriod     = 6;
        openRatioThreshold   = 0.3;   
        askRatioThreshold    = 0.6;
        minTradedVolume      = 3;
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        limitClosePrice
        % storage
        values
        snapshot
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        pendingOrderTime     
        fillPrice;
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        
        
        
        pnl = 0;
        cumPnl = 0;
        
    end
    
    methods
        
        function this = intradayRevLite()
            
            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.pendingOrderTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
       
            this.values = NaN(this.lbw, 1);
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            % status are not updated in this function 
            
            this.snapshot = snapshot;
            this.values = [this.values(2:end); snapshot.Mid];
            %display(['curStatus is: ',num2str(this.curStatus) ]);
            
            switch this.curStatus
                
                case status.hasNothing  
                    
                    openRatio = snapshot.OpenVolume/snapshot.Volume;
                    askRatio = snapshot.AskSize/(snapshot.AskSize + snapshot.BidSize);
                    bidRatio = snapshot.BidSize/(snapshot.AskSize + snapshot.BidSize);
                    
                    % to-do: tradeImb
                    if ~isnan(this.values)
                        
                        tau = MKT2(this.values);
                    
                        % if up-trend && reverse 
                        if tau > this.mktThreshold ...
                           && openRatio < this.openRatioThreshold ...
                           && snapshot.Volume > this.minTradedVolume % && askRatio > 0.6

                            if this.enterType == 0 % IOC in
                                display([datestr(this.snapshot.Time),' up-trend reverse, IOC open short position ']);
                                this.SellOpen(InstrumentID, 1, snapshot.Bid(1), true); 
                            else        
                                display([datestr(this.snapshot.Time),' up-trend reverse, limit open short position ']);
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                            end
                            
                             
                        % if down-trend && reverse 
                        elseif tau < (-1)*this.mktThreshold ...
                               && openRatio < this.openRatioThreshold ...
                               && snapshot.Volume > this.minTradedVolume %&& bidRatio > 0.6
                            
                            if this.enterType == 0 % IOC in
                                display([datestr(this.snapshot.Time),' down-trend reverse, IOC open long position ']);
                                this.BuyOpen(InstrumentID, 1, snapshot.Ask(1), true);                           
                            else
                                display([datestr(this.snapshot.Time),' down-trend reverse, limit open long position ']);
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                            end
                            

                        end
                    end
                        
                case status.hasOnePendingOrder
                    
                    this.pendingOrderTime =  this.pendingOrderTime + 1;
                    
                    if this.pendingOrderTime >= this.maxPendingPeriod 
                        
                        display([datestr(this.snapshot.Time),' pending order max life reached ']);
                        
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                        %disp(['position holding time is ',num2str(this.positionHoldingTime)]);
                    
                    % time filter 
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 1: limit close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                            this.limitClosePrice = snapshot.Ask(1);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter 1: limit close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                            this.limitClosePrice = snapshot.Bid(1);
                            
                        end
                        
                        this.curStatus = 3;
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    % pnl filter 
                    if this.liveAmount > 0
                        pnl = snapshot.Bid(1) - this.fillPrice;
                    elseif this.liveAmount < 0
                        pnl = this.fillPrice - snapshot.Ask(1);
                    end
                    
                    
                    if this.liveAmount > 0 && pnl >= this.takeprofitLevel
                        
                            display([datestr(this.snapshot.Time),' take-profit: IOC close long position']);
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl >= this.takeprofitLevel
                            
                            display([datestr(this.snapshot.Time),' take-profit: IOC close short position']);
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    elseif this.liveAmount > 0 && pnl <= this.stoplossLevel
                        
%                             display([datestr(this.snapshot.Time),' stop-loss: IOC close long position']);
%                             this.CancelAllOrder();
%                             this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl <= this.stoplossLevel
                            
%                             display([datestr(this.snapshot.Time),' stop-loss: IOC close short position']);
%                             this.CancelAllOrder();
%                             this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    end
                       
                    
                case status.hasOnePositionOnePendingOrder 
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                   
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                            
                            if this.limitClosePrice ~= snapshot.Ask(1);
                                
                                display([datestr(this.snapshot.Time),' update limit close long order! ']);
                                
                                this.CancelAllOrder();

                                this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                                this.limitClosePrice = snapshot.Ask(1);
                            end
                            
                        elseif this.liveAmount < 0 
                            
                            if this.limitClosePrice ~= snapshot.Bid(1);
                                
                                display([datestr(this.snapshot.Time),'  update limit close short order! ']);
                                
                                this.CancelAllOrder();
                                
                                this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                                this.limitClosePrice = snapshot.Bid(1);
                            end
                            
                        end
                        
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    
            end
            
        end
            
       

        
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.pendingOrderTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.pendingOrderTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, Order )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;
                    
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )
            
            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
            else
                this.liveAmount = this.liveAmount - Amount;
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    display([datestr(this.snapshot.Time),' IOC IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.pendingOrderTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' IOC OUT success ', ' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
    end
    
end