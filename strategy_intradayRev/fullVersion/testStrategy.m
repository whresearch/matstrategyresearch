clc
clear
close all
tic

% load data
StartDate = datetime(2015, 10, 12);
EndDate = datetime(2015, 10, 12);
Data = LoadMat_SHFE_Range('cu1512', StartDate, EndDate);
MultiThread = true;

% create strategy
Strategy = intradayRev();

% calculate results
Result = Backtest(Data, Strategy, MultiThread,3,false);

Result.ShowChart();
toc;
