classdef intradayRev < Strategy
    
    properties

        % strategy parameters 
        lbw                  = 30;
        mktThreshold         = 0.65;
        takeprofitLevel      = 100;
        stoplossLevel        = -40;
        maxHoldingPeriod1    = 200;
        maxHoldingPeriod2    = 300;
        maxPendingPeriod     = 6;
        openRatioThreshold   = 0.3;   
        askRatioThreshold    = 0.6;
        minTradedVolume      = 3;
        
        limitClosePrice
        % storage
        values
        snapshot
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        pendingOrderTime     
        fillPrice;
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order

        pnl = 0;
        cumPnl = 0;
        numTrade =0;
        
    end
    
    methods
        
        function this = intradayRev()
            
            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.pendingOrderTime = 0;
            this.fillPrice = 0;
            this.curStatus = 0;
       
            this.values = NaN(this.lbw, 1);
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            
            this.snapshot = snapshot;
            this.values = [this.values(2:end); snapshot.Mid];
            %display(['curStatus is: ',num2str(this.curStatus) ]);
            
            switch this.curStatus
                
                case 0 % has nothing 
                    
                    openRatio = snapshot.OpenVolume/snapshot.Volume;
                    askRatio = snapshot.AskSize/(snapshot.AskSize + snapshot.BidSize);
                    bidRatio = snapshot.BidSize/(snapshot.AskSize + snapshot.BidSize);
                    
                    % to-do: tradeImb
                    if ~isnan(this.values)
                        
                        tau = MKT2(this.values);
                    
                        % if up-trend && reverse 
                        if tau > this.mktThreshold ...
                           && openRatio < this.openRatioThreshold ...
                           && snapshot.Volume > this.minTradedVolume %&& askRatio > 0.6

                            display([datestr(this.snapshot.Time),' up-trend reverse, limit open short position ']);

                            this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); this.curStatus = 1;

                        % if down-trend && reverse 
                        elseif tau < (-1)*this.mktThreshold ...
                               && openRatio < this.openRatioThreshold ...
                               && snapshot.Volume > this.minTradedVolume %&& bidRatio > 0.6

                            display([datestr(this.snapshot.Time),' down-trend reverse, limit open long position ']);

                            this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); this.curStatus = 1;

                        end
                    end
                        
                case 1 % has pending order
                    
                    this.pendingOrderTime =  this.pendingOrderTime + 1;
                    
                    if this.pendingOrderTime >= this.maxPendingPeriod 
                        
                        display([datestr(this.snapshot.Time),' pending order maximum life reached ']);
                        
                        this.CancelAllOrder();
                        this.curStatus = 0;

                    end
                  
                    
                case 2 % has position
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                        %disp(['position holding time is ',num2str(this.positionHoldingTime)]);
                    
                    % time filter 
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 1: limit close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                            this.limitClosePrice = snapshot.Ask(1);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter 1: limit close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                            this.limitClosePrice = snapshot.Bid(1);
                            
                        end
                        
                        this.curStatus = 3;
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 2 : ioc close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter2 : ioc close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    % pnl filter 
                    if this.liveAmount > 0
                        pnl = snapshot.Bid(1) - this.fillPrice;
                    elseif this.liveAmount < 0
                        pnl = this.fillPrice - snapshot.Ask(1);
                    end
                    
                    
                    if this.liveAmount > 0 && pnl >= this.takeprofitLevel
                        
                            display([datestr(this.snapshot.Time),' take-profit: close long position']);
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl >= this.takeprofitLevel
                            
                            display([datestr(this.snapshot.Time),' take-profit: close short position']);
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    elseif this.liveAmount > 0 && pnl <= this.stoplossLevel
                        
                            display([datestr(this.snapshot.Time),' stop-loss: close long position']);
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl <= this.stoplossLevel
                            
                            display([datestr(this.snapshot.Time),' stop-loss: close short position']);
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    end
                        
                    % to do: limit out
                    
                    
                case 3 % has position and pending order
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                   
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                            
                            if this.limitClosePrice ~= snapshot.Ask(1);
                                
                                this.CancelAllOrder();
                                
                                display([datestr(this.snapshot.Time),' limit close long position updated! ']);
                            
                                this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                                this.limitClosePrice = snapshot.Ask(1);
                            end
                            
                        elseif this.liveAmount < 0 
                            
                            if this.limitClosePrice ~= snapshot.Bid(1);
                            
                                this.CancelAllOrder();
                                
                                display([datestr(this.snapshot.Time),' limit close short position updated! ']);
                            
                                this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                                this.limitClosePrice = snapshot.Bid(1);
                            end
                            
                        end
                        
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter 2 : ioc close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter2 : ioc close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    
                    
                    % to do 
                    
            end
            
        end
            
       

        
        function OnOrderInsertAck( this, Order )
            
            display([datestr(this.snapshot.Time),' limit order ack']);
            
            if this.curStatus == 0
                this.curStatus = 1; % status from 0 to 1
            end
            
            this.pendingOrderTime = 0;
            
        end
        
        function OnOrderCancel( this, Order )
            
            display([datestr(this.snapshot.Time),' pending or ioc order cancelled ']);
            
            if this.curStatus == 1
                this.curStatus = 0; % status from 1 to 0   
            end
            
        end
        
        function OnTrade( this, Order, Amount, Price )
            
            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
            else
                this.liveAmount = this.liveAmount - Amount;
            end
            
            switch this.curStatus
                
                case 0 
                    
                    display([datestr(this.snapshot.Time),' IOC filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.curStatus = 2;

                    
                case 1
                    
                    display([datestr(this.snapshot.Time),' limit order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.pendingOrderTime = 0;
                    
                    this.curStatus = 2;
                    
                    
                case 2
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' position closed from status 2', ' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = 0;
                    
                    this.fillPrice = 0;
                   
 
                    
                case 3
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' position closed from status 3', ' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    
                    this.curStatus = 0;
                    
                    this.fillPrice = 0;
                    
                    % to-do
                    
                    
            end

        end
        
    end
    
end

