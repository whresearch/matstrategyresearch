clc
clear
close all

tic
% load data
% StartDate = datetime(2015, 9, 10);
% EndDate = datetime(2015, 9, 10);
% Data = LoadMat_SHFE_Range('cu1511', StartDate, EndDate);

Data = LoadMat_SHFE_CU_Main(136);

% create strategy
% Strategy = intradayRevLite();
 Strategy = tradesTrackerLite(0.88, 80, 10, 30);
% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();

Btl.showResults();
Btl.sharpe

plot(cumsum(Btl.pnlVec));

%tableViewLite
toc
