function [ sizeBuy, sizeSell ] = tradeDirection( bidPx, askPx, bidSz, askSz, vwap, volume, tickSize )
%TRADEDIRECTION Summary of this function goes here
%   Detailed explanation goes here

    vwap(volume==0)=nan;
    
    [tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPx, askPx, bidSz, askSz, vwap, volume, tickSize );

    sideCondBuy = (tradeSide==1);
    sideCondSell = (tradeSide==0);
    
    tradePxBuy = zeros (size(tradePx));
    tradePxSell = zeros (size(tradePx));
    
    tradePxBuy(sideCondBuy) = tradePx (sideCondBuy);
    tradePxSell(sideCondSell) = tradePx (sideCondSell);

    
    tradeSizeBuy = zeros (size(tradeSize));
    tradeSizeSell = zeros (size(tradeSize));
    
    tradeSizeBuy(sideCondBuy) = tradeSize (sideCondBuy);
    tradeSizeSell(sideCondSell) = tradeSize (sideCondSell);
    
    sizeBuy = sum (tradeSizeBuy, 2);
    sizeSell = sum (tradeSizeSell, 2);
    
    avgPxBuy=sum(tradePxBuy.*tradeSizeBuy, 2)./sizeBuy;
    avgPxSell=sum(tradePxSell.*tradeSizeSell, 2)./sizeSell;
end

