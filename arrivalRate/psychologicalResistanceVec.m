
function [ bidCond, askCond ] = psychologicalResistanceVec(lookBwd, bidPx, askPx, bidSz, askSz,  bidSzLimit, askSzLimit, buySz, sellSz, tradeImbLimit )
%PSYCHOLOGICALRESISTANCEVEC Summary of this function goes here
%   Detailed explanation goes here

n = size (bidPx, 1);
askCond = zeros(size(bidPx));
bidCond = zeros(size(bidPx));

for i=lookBwd:n
    bidCond(i) = psychologicalResistance (true, bidPx(i-lookBwd+1:i), bidSz(i-lookBwd+1:i), bidSzLimit, buySz(i-lookBwd+1:i), sellSz(i-lookBwd+1:i), tradeImbLimit);
    askCond(i) = psychologicalResistance (false, askPx(i-lookBwd+1:i), askSz(i-lookBwd+1:i), askSzLimit, buySz(i-lookBwd+1:i), sellSz(i-lookBwd+1:i), tradeImbLimit);
end

end

