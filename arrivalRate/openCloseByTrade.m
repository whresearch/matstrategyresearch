function [ open, close ] = openCloseByTrade( volume, openVol, closeVol, sizeBuy, sizeSell )
%OPENCLOSEBYTRADE Summary of this function goes here
%   Detailed explanation goes here

    allVolIsBuy = (sizeBuy == volume);
    allVolIsSell = (sizeSell == volume);
    
    open = zeros (size(openVol, 1), 2);
    open(allVolIsBuy,1) = openVol (allVolIsBuy);
    open(allVolIsSell,2) = openVol (allVolIsSell);
    
    close = zeros (size(closeVol, 1), 2);
    close(allVolIsBuy,1) = closeVol (allVolIsBuy);
    close(allVolIsSell,2) = closeVol (allVolIsSell);
    
end

