function [ fillMatBuy, fillMatSell, avgPxBuy, avgPxSell, sizeBuy, sizeSell ] = fillRate( bidPx, askPx, bidSz, askSz, vwap, volume, tickSize, lookFwd )
%FILLRATE Summary of this function goes here
%   Detailed explanation goes here
    maxLife = lookFwd;
    
    %vwap = fixBlankVwap (vwap, volume);
    vwap(volume==0)=nan;
    
    [tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPx, askPx, bidSz, askSz, vwap, volume, tickSize );
    
    
    %tmp = table(tradePx, tradeSize, tradeSide,  bidSz, bidPx,  vwap, askPx, askSz, volume);
    
    
%     bidVol = bidVol(2:end);
%     askVol = askVol(2:end);
    
    numTicks = size(bidPx, 1);
    ticks = [1:numTicks]';
    tickCols = repmat(ticks, 1, size(tradePx, 2));
    
    maxPx = max (tradePx(:));
    minPx = min (tradePx(:));
    pxRows = (minPx:maxPx)';
    numPx = (maxPx - minPx)/tickSize + 1;
    
    

    
    
    sideCondBuy = (tradeSide==1);
    sideCondSell = (tradeSide==0);
    
    tradePxBuySerialised = tradePx (sideCondBuy);
    tradePxSellSerialised = tradePx (sideCondSell);
    
    tradePxBuy = zeros (size(tradePx));
    tradePxSell = zeros (size(tradePx));
    
    tradePxBuy(sideCondBuy) = tradePx (sideCondBuy);
    tradePxSell(sideCondSell) = tradePx (sideCondSell);
    
    
    tradeSizeBuySerialised = tradeSize (sideCondBuy);
    tradeSizeSellSerialised = tradeSize (sideCondSell);
    
    tradeSizeBuy = zeros (size(tradeSize));
    tradeSizeSell = zeros (size(tradeSize));
    
    tradeSizeBuy(sideCondBuy) = tradeSize (sideCondBuy);
    tradeSizeSell(sideCondSell) = tradeSize (sideCondSell);
    
    sizeBuy = sum (tradeSizeBuy, 2);
    sizeSell = sum (tradeSizeSell, 2);
    
    avgPxBuy=sum(tradePxBuy.*tradeSizeBuy, 2)./sizeBuy;
    avgPxSell=sum(tradePxSell.*tradeSizeSell, 2)./sizeSell;
    
    
    
    %bidAskSizeRatio = bidSz./askSz;
    %buySellRatio = sizeBuy./(sizeSell);
    
    %scatter (bidAskSizeRatio(1:end-1), buySellRatio(2:end));
    
    tradeTimeBuy = tickCols (sideCondBuy);
    tradeTimeSell = tickCols (sideCondSell);
    
    
    fillMatIdxBuy = (tradePxBuySerialised - minPx)/tickSize + 1;
    fillMatIdxSell = (tradePxSellSerialised - minPx)/tickSize + 1;
    
    fillMatBuy = zeros (numPx, numTicks);
    fillMatSell = zeros (numPx, numTicks);
    
    indBuy = sub2ind(size(fillMatBuy),fillMatIdxBuy, tradeTimeBuy);
    indSell = sub2ind(size(fillMatSell),fillMatIdxSell, tradeTimeSell);
    

    %TODO +=
    fillMatBuy(indBuy) = tradeSizeBuySerialised;
    fillMatSell(indSell) = tradeSizeSellSerialised;
    
    
    
    
    

    
%     bidVolWin = createRollingWindow (bidVol, lookFwd);
%     askVolWin = createRollingWindow (askVol, lookFwd);
%     
%     bidPx2 = bidPx(1:end-1);
%     askPx2 = askPx(1:end-1);
%     
%     bidWin = createRollingWindow (bidPx2, lookFwd);
%     askWin = createRollingWindow (askPx2, lookFwd);
%     
%     
%     %bidLife = priceLevelLife (bidPx, maxLife, true);
%     %askLife = priceLevelLife (askPx, maxLife, false);
%     
%     bidFirstCol = repmat (bidWin(:,1), 1, size(bidWin, 2));
%     askFirstCol = repmat (askWin(:,1), 1, size(askWin, 2));
%     
%     bidPxLvlHit = bidFirstCol - bidWin;
%     askPxLvlHit = askWin - askFirstCol;
%     
%     [~, bidIdx] = max (bidPxLvlHit, [], 2);
%     [~, askIdx] = max (askPxLvlHit, [], 2);
% 
% 
%     bidIdx (bidIdx == 1) = maxLife;
%     askIdx (askIdx == 1) = maxLife;
%     
%     bidLife = bidIdx;
%     askLife = askIdx;
end

