function [ result ] = psychologicalResistance( isBid, px, sz, szLimit, buySz, sellSz, tradeImbLimit )
%PSYCHOLOGICALRESISTANCE Summary of this function goes here
%   Detailed explanation goes here

tradeImb = mean(buySz - sellSz);

uniquePx = unique (px);

if length(uniquePx)~=2
    result = false;
    return
end

maxPx = max(px);
minPx = min(px);

if isBid
    resistancePx = minPx;
else
    resistancePx = maxPx;
end

pxCond = px == resistancePx;

occourance = sum(pxCond);
if occourance ~=1
    result = false;
    return;
end

idx = find (pxCond, 1, 'last');

if idx==length(px) || idx==1
    result = false;
    return;
end

size=sz(idx);

if isBid
    szCond = size>szLimit;
else
    szCond = size>szLimit;
end

if isBid
    tradeImbCond = tradeImb>tradeImbLimit;
else
    tradeImbCond = tradeImb<-tradeImbLimit;
    
end

if ~szCond && ~tradeImbCond
    result = false;
    return;
end


result = true;
end

