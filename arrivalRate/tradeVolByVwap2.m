function [sizeBuy, sizeSell, sizeUnknown, pxHigh, pxLow, qtyHigh, qtyLow] = tradeVolByVwap2( preBidPx, preAskPx, vwap, volume, tickSize )
    % deal with single snapshot 
    volume = volume/2;
    sizeBuy = 0;
    sizeSell = 0;
    sizeUnknown = 0;
    vwap(isnan(vwap))=0;
    
    % get the nearest two prices from vwap 
    pxHigh = ceil(vwap/tickSize)*tickSize;
    pxLow = floor(vwap/tickSize)*tickSize;
    
    preMid = (preBidPx + preAskPx)/2;

    if pxHigh == pxLow
        
        if pxHigh > preMid
            sizeBuy = volume;
        elseif pxHigh < preMid
            sizeSell = volume;
        else
            sizeUnknown = volume;
        end
        
        qtyHigh = volume; % volume is assigned to qtyHigh 
        qtyLow = 0;
        
    else
        
        ratio = (vwap-pxLow)/tickSize;
        qtyHigh = volume*ratio;
        qtyLow = volume*(1-ratio);

        if pxHigh > preMid
            sizeBuy = sizeBuy + qtyHigh;
        elseif pxHigh < preMid
            sizeSell = sizeSell + qtyHigh;
        else
            sizeUnknown = sizeUnknown + qtyHigh;
        end
        
        if pxLow > preMid
            sizeBuy = sizeBuy + qtyLow;
        elseif pxLow < preMid
            sizeSell = sizeSell + qtyLow;
        else
            sizeUnknown = sizeUnknown + qtyLow;
        end
    
    end

end

