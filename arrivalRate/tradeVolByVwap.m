function [tradePx, tradeSize, tradeSide] = tradeVolByVwap( bidPx, askPx, bidSz, askSz, vwap, volume, tickSize )
%tradeVolByVwap Summary of this function goes here
%   Detailed explanation goes here
    volume = volume/2;
    pxNearestHigher = ceil(vwap/tickSize)*tickSize;
    pxNearestLower = floor(vwap/tickSize)*tickSize;
    
    %eqCond = (pxNearestLower == pxNearestHigher);
    
    mid = (bidPx + askPx)/2;
    midPrior = [NaN; mid(1:end-1)];
    
    tradePx = [pxNearestLower pxNearestHigher];
    
    repMidPrior = repmat (midPrior, 1, size(tradePx, 2));
    tradeSide = NaN (size(tradePx));
    tradeSide (tradePx > repMidPrior) = 1;
    tradeSide (tradePx < repMidPrior) = 0;
    if(sum(tradePx == repMidPrior)>=1)
        %warning ('there are trades with undetermined direction -> tradePx == repMidPrior !!!!!');
        %equality = tradePx == repMidPrior;
        %tmp = table (equality, tradePx, repMidPrior, bidPx, askPx, bidSz, askSz, vwap, volume);
    end
    %TODO >, < mid is not sufficient
    
    
    ratio = (vwap - pxNearestLower) ./ (pxNearestHigher - pxNearestLower);
    
    ratioMat = [(1-ratio) ratio];
    
    identicalCond = (tradePx(:,1) == tradePx(:,2));
    
    ratioMat(identicalCond,1) = 1;
    ratioMat(identicalCond,2) = NaN;
    tradePx(identicalCond,2) = NaN;
    tradeSide(identicalCond,2) = NaN;
    
    repVol = repmat (volume, 1, size(tradePx, 2));
    tradeSize =  ratioMat .* repVol;
    
    %ratio = (vwap - bidPx) ./ (askPx - bidPx);
    %ratio(ratio < 0) = 0;
    %ratio(ratio > 1) = 1;
    %askVol = ratio .* volume;
    %bidVol = (1-ratio) .* volume;

end

