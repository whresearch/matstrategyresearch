function [sizeBuy, sizeSell, sizeUnknown] = tradeVolByVwap2_vec( bid, ask, vwap, volume, tickSize )

n = length(bid);

sizeBuy = zeros(n,1);
sizeSell = zeros(n,1);
sizeUnknown = zeros(n,1);
sizeUnknown(1) = volume(1)/2;

for i = 2 : n
    
    [sizeBuy(i), sizeSell(i), sizeUnknown(i)] = tradeVolByVwap2(bid(i-1),ask(i-1),vwap(i),volume(i),tickSize);
    
    
end
    


end