tickSize = 10;
days=8:60;
numDays = length (days);
lookFwd = 200;
maxQueuePos = 60;
accumSize=1;

ordImbThreshold = 0.7;
trdImbThreshold = 0.7;
minTradeSize = 10;

for i = days
    disp (['day: ' num2str(i)]);
    Data = LoadMat_SHFE_CU_Main(i);
    disp (['loaded: ' num2str(i)]);
    
    Data.Bid=Data.Bid(1:57600);
    Data.Ask=Data.Ask(1:57600);
    Data.BidSize=Data.BidSize(1:57600);
    Data.AskSize=Data.AskSize(1:57600);
    Data.VWAP=Data.VWAP(1:57600);
    Data.Volume=Data.Volume(1:57600);
    
    mid = (Data.Bid + Data.Ask) / 2;
    [fillMatBuy, fillMatSell, avgPxBuy, avgPxSell, sizeBuy, sizeSell] = fillRate( Data.Bid, Data.Ask, Data.BidSize, Data.AskSize, Data.VWAP, Data.Volume, tickSize, lookFwd );
   
    
    bidSz = sum(reshape(Data.BidSize, accumSize, []), 1)';
    askSz = sum(reshape(Data.AskSize, accumSize, []), 1)';
    midRe = reshape(mid, accumSize, [])';
    mid = midRe(:, end);
    
    ordImb = bidSz./(bidSz + askSz);
    ordImbCond = ordImb > ordImbThreshold;

    
    sizeBuy = sum(reshape(sizeBuy, accumSize, []), 1)';
    sizeSell = sum(reshape(sizeSell, accumSize, []), 1)';
    
    minTrdSizeCond = sizeBuy > minTradeSize;
    trdImb = sizeBuy./(sizeBuy + sizeSell);
    %trdImbCond = trdImb > arrImbThreshold;
    
    trdImbCond = trdImb > trdImbThreshold;
    
    trdImbCond = trdImbCond & minTrdSizeCond;
    

    
    tmp2= table ( Data.Bid, Data.Ask, mid, Data.BidSize, Data.AskSize, Data.VWAP, Data.Volume, sizeBuy, sizeSell, trdImbCond);
    
    imb = [ordImbCond trdImbCond];
    %theCond = ordImbCond;
    theCond = trdImbCond;
    %theCond = trdImbCond & ordImbCond;
    
    numOrdImbCond = sum (ordImbCond);
    numTrdImbCond = sum (trdImbCond);
    numAnd = sum (theCond);
    ratioAndOrd = numAnd / sum (ordImbCond);
    ratioAndTrd = numAnd / sum (trdImbCond);
    
    %tmp= table (numOrdImbCond, numTrdImbCond, numAnd, ratioAndOrd, ratioAndTrd)
    
    midWin = createRollingWindow (mid, lookFwd);
    
    baseWin = repmat (midWin(:,1), 1, size(midWin,2));
    
    impactWin = midWin-baseWin;
    

    
    imbCondResized  = theCond (1:size(midWin,1));
    
    impactWinDropCond = impactWin (:, 2) < 0;
    
    impactWinCond = imbCondResized;% & impactWinDropCond;
    
    impactWinSelected = impactWin (impactWinCond, :);
    
    impactMean = mean (impactWinSelected, 1);
    
    tmp = impactWinSelected(:, 2);
    tmp2 = mean(impactWinSelected(:, 2));
    tmp3 = mean(impactWinSelected(:, 3));
    
    diff = impactWinSelected(:, 3) - impactWinSelected(:, 2);
    
    tmpdiff = mean (diff);
    
    histogram(tmp, -20:20);
    
    %hold on
    %plot (incrMean);
end