function [ passiveOrderBid, passiveOrderAsk ] = passiveLiquidity( bidPrice, askPrice, bidSize, askSize, preBid, preAsk, preBidSize, preAskSize, pxHigh, pxLow, qtyHigh, qtyLow )
%PASSIVELIQUIDITY Summary of this function goes here
%   Detailed explanation goes here

passiveOrderAsk = 0;
passiveOrderBid = 0;

if pxHigh == pxLow

    if askPrice<preAsk
        passiveOrderAsk = askSize;        
    elseif askPrice==preAsk
        passiveOrderAsk = askSize-preAskSize;
        if askPrice==pxHigh
            passiveOrderAsk = passiveOrderAsk + qtyHigh;
        end

    else %
        if preAsk==pxHigh
            passiveOrderAsk = qtyHigh - preAskSize;
        end
    end
    
    
    if bidPrice>preBid
        passiveOrderBid = bidSize;
    elseif bidPrice==preBid
        passiveOrderBid = bidSize-preBidSize;
        if bidPrice==pxHigh
            passiveOrderBid = passiveOrderBid + qtyHigh;
        end

    else
        if preBid==pxHigh
            passiveOrderBid = qtyHigh - preBidSize;
        end

    end
    
else
    
    if askPrice<preAsk
        passiveOrderAsk = askSize;        
    elseif askPrice==preAsk
        passiveOrderAsk = askSize-preAskSize;
        if askPrice==pxHigh
            passiveOrderAsk = passiveOrderAsk + qtyHigh;
        end
        if askPrice==pxLow
            passiveOrderAsk = passiveOrderAsk + qtyLow;
        end
    else %
        if preAsk==pxHigh
            passiveOrderAsk = qtyHigh - preAskSize;
        end
        if preAsk==pxLow
            passiveOrderAsk = qtyLow - preAskSize;
        end
    end
    
    
    if bidPrice>preBid
        passiveOrderBid = bidSize;
    elseif bidPrice==preBid
        passiveOrderBid = bidSize-preBidSize;
        if bidPrice==pxHigh
            passiveOrderBid = passiveOrderBid + qtyHigh;
        end
        if bidPrice==pxLow
            passiveOrderBid = passiveOrderBid + qtyLow;
        end
    else
        if preBid==pxHigh
            passiveOrderBid = qtyHigh - preBidSize;
        end
        if preBid==pxLow
            passiveOrderBid = qtyLow - preBidSize;
        end
    end
    
end

