Data = LoadMat_SHFE('shfe_20160108_cu1603.mat');

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;
spread = askPrice - bidPrice;

lbw = 30;

askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

tradeImb = sizeBuy - sizeSell;

[passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec (bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);

[bidResist, askResist] = psychologicalResistanceVec (6, bidPrice, askPrice, bidSize, askSize, 100,100, sizeBuy, sizeSell, 30);

orderTable = table(time, bidResist, passiveOrderBid, bidSize, bidPrice, askPrice, askSize, passiveOrderAsk, askResist, volume, tradeImb, tradePx(:,1),tradeSize(:,1), tradePx(:,2),tradeSize(:,2), VWAP);




