function [ passiveOrderBid, passiveOrderAsk ] = passiveLiquidityVec( bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize )
%PASSIVELIQUIDITY Summary of this function goes here
%   Detailed explanation goes here

passiveOrderAsk = nan(length(askPrice),1);
passiveOrderBid = nan(length(bidPrice),1);

for i=2:length (askPrice)
    if askPrice(i)<askPrice(i-1)
        passiveOrderAsk(i) = askSize(i);        
    elseif askPrice(i)==askPrice(i-1)
        passiveOrderAsk(i) = askSize(i)-askSize(i-1);
        if askPrice(i)==tradePx(i,1)
            passiveOrderAsk(i) = passiveOrderAsk(i) + tradeSize(i,1);
        end
        if askPrice(i)==tradePx(i,2)
            passiveOrderAsk(i) = passiveOrderAsk(i) + tradeSize(i,2);
        end
    else %
        if askPrice(i-1)==tradePx(i,1)
            passiveOrderAsk(i) = tradeSize(i,1) - askSize(i-1);
        end
        if askPrice(i-1)==tradePx(i,2)
            passiveOrderAsk(i) = tradeSize(i,2) - askSize(i-1);
        end
    end
    
    
    if bidPrice(i)>bidPrice(i-1)
        passiveOrderBid(i) = bidSize(i);
    elseif bidPrice(i)==bidPrice(i-1)
        passiveOrderBid(i) = bidSize(i)-bidSize(i-1);
        if bidPrice(i)==tradePx(i,1)
            passiveOrderBid(i) = passiveOrderBid(i) + tradeSize(i,1);
        end
        if bidPrice(i)==tradePx(i,2)
            passiveOrderBid(i) = passiveOrderBid(i) + tradeSize(i,2);
        end
    else
        if bidPrice(i-1)==tradePx(i,1)
            passiveOrderBid(i) = tradeSize(i,1) - bidSize(i-1);
        end
        if bidPrice(i-1)==tradePx(i,2)
            passiveOrderBid(i) = tradeSize(i,2) - bidSize(i-1);
        end
    end
end
% TO DO, why it can be nan?
passiveOrderAsk(isnan(passiveOrderAsk)) = 0;

passiveOrderBid(isnan(passiveOrderBid)) = 0;

end

