% dataNum=96;

%Data = LoadMat_SHFE_CU_Main(dataNum); 

%Data = LoadMat_SHFE('shfe_20160119_cu1603.mat');

tradeImbThreshold = 550; % 40 for CU; 650 for rRB
bidRatioThreshold = 0.75;

Data = LoadMat_SHFE('shfe_20160121_rb1605.mat');

tickSize = Data.Contract.TickSize;

% clear dataNum

%Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;


%plot(cumTI);
%plotyy(1:length(mid),cumTradeImb,1:length(mid),mid);


% impulse response on tradeImb
signal = zeros(size(bidPrice));


askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

tradeImb = sizeBuy - sizeSell;
tradeImb(isnan(tradeImb))=0;
cumTradeImb = cumsum(tradeImb);

[passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec (bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);

[bidResist, askResist] = psychologicalResistanceVec (6, bidPrice, askPrice, bidSize, askSize, 100,100, sizeBuy, sizeSell, 30);


tradeImbRange=  [400:10:700];
bidRatioRange = [0.60:0.05:1];
passiveImbRange = [-200:10:200];

combs = combvec(tradeImbRange, bidRatioRange, passiveImbRange);

right = nan (size(combs,1), 4);

passiveImb = passiveOrderBid - passiveOrderAsk;

for i=1:size(combs,2)
    tradeImbParam = combs(1,i);
    bidRatioParam = combs(2,i);
    passiveImbParam = combs(3,i);
    signal = zeros(size(bidPrice));
    signal (tradeImb > tradeImbParam & bidRatio > bidRatioParam & passiveImb >passiveImbParam) = 1 ;
    signal (tradeImb < -tradeImbParam & bidRatio < 1 - bidRatioParam &  passiveImb < -passiveImbParam) = -1;
    [upRight, upNum, downRight, downNum] = impulseResponse (signal, mid);
    right(i,:) = [upRight, upNum, downRight, downNum];
end

overview = table(right, combs');

figure
hold on
total = right(:,2) + right(:,4);
totalRight = right(:,2).*right(:,1) + right(:,4).*right(:,3);

%scatter (right(:,2), right(:,1));
%scatter (right(:,4), right(:,3));
scatter (total, totalRight./total);


orderTable = table(time, bidResist, passiveOrderBid, bidSize, bidPrice, askPrice, askSize, passiveOrderAsk, askResist, volume, signal, tradeImb, passiveImb, VWAP);





