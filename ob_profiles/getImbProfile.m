clc
clear
Data = LoadMat_SHFE('shfe_20160119_cu1603.mat');

tradeImbThreshold = 40;
bidRatioThreshold = 0.85;

get_imbSignal_profile(Data, tradeImbThreshold, bidRatioThreshold);