
% dataNum=96;

%Data = LoadMat_SHFE_CU_Main(dataNum); 

%Data = LoadMat_SHFE('shfe_20160119_cu1603.mat');

tradeImbThreshold = 550; % 40 for CU; 650 for rRB
bidRatioThreshold = 0.75;

Data = LoadMat_SHFE('shfe_20160119_rb1605.mat');

tickSize = Data.Contract.TickSize;

% clear dataNum

%Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;


%plot(cumTI);
%plotyy(1:length(mid),cumTradeImb,1:length(mid),mid);


% impulse response on tradeImb
signal = zeros(size(bidPrice));


askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

tradeImb = sizeBuy - sizeSell;
tradeImb(isnan(tradeImb))=0;
cumTradeImb = cumsum(tradeImb);

[passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec (bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);

[bidResist, askResist] = psychologicalResistanceVec (6, bidPrice, askPrice, bidSize, askSize, 100,100, sizeBuy, sizeSell, 30);




passiveImb = passiveOrderBid - passiveOrderAsk;

signal (tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold ) = 1 ;
signal (tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold ) = -1;
impulseResponse (signal, mid);

signal = zeros(size(bidPrice));
signal (tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold & passiveImb>00) = 1 ;
signal (tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold & passiveImb<-00) = -1;
impulseResponse (signal, mid);



orderTable = table(time, bidResist, passiveOrderBid, bidSize, bidPrice, askPrice, askSize, passiveOrderAsk, askResist, volume, signal, tradeImb, passiveImb, VWAP);





