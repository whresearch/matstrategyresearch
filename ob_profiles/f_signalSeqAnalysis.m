function  f_signalSeqAnalysis( signals )
%F_SIGNALSEQANALYSIS Summary of this function goes here
%   Detailed explanation goes here
pos_ratio = sum(signals>0)/length(signals);
neg_ratio = sum(signals<0)/length(signals);

disp(['pos signal ratio: ', num2str(pos_ratio)]);

disp(['neg signal ratio: ', num2str(neg_ratio)]);


pos2pos = 0;
pos2neg = 0;
neg2pos = 0;
neg2neg = 0;

for i = 1:length(signals)-1
    
   if signals(i) == 1 && signals(i+1) == 1
       pos2pos = pos2pos + 1;
   elseif signals(i) == 1 && signals(i+1) == -1
       pos2neg = pos2neg + 1;
   elseif signals(i) == -1 && signals(i+1) == 1
       neg2pos = neg2pos + 1;
   elseif signals(i) == -1 && signals(i+1) == -1
       neg2neg = neg2neg + 1;
   end
        
end



disp(['pos2pos: ', num2str(pos2pos)]);
disp(['pos2neg: ', num2str(pos2neg)]);
disp(['neg2pos: ', num2str(neg2pos)]);
disp(['neg2neg: ', num2str(neg2neg)]);


end

