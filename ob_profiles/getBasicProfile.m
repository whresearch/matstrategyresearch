clc
clear

Data = LoadMat_SHFE('shfe_20160311_cu1605.mat');


tickSize = Data.Contract.TickSize;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;
last = Data.Last;

bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

% trade imbalance
tradeImb = sizeBuy - sizeSell;
tradeImb(isnan(tradeImb))=0;

disp(['data length: ', num2str(length(mid))]);

disp(['daily volume: ', num2str(sum(volume))]);

dailyChange = last(end)/last(1) - 1; 
dailyChange_inTicks = (last(end) - last(1))/tickSize;
% general info
disp(['daily change: ', num2str(dailyChange), ' = ', num2str(dailyChange_inTicks), ' ticks']);

dailyRange = (max(last) - min(last))/tickSize;

disp(['daily range: ', num2str(dailyRange), ' ticks']);

fprintf('\rbidSize: \r'); 
get_stats(bidSize)

fprintf('\raskSize: \r'); 
get_stats(askSize)

% tradeImb info
posTradeImb = tradeImb(tradeImb > 0);
fprintf('\rPositive tradeImb: \r'); 
get_stats(posTradeImb);
disp(['occurrence: ',num2str(length(posTradeImb)/length(tradeImb))])

negTradeImb = tradeImb(tradeImb < 0);
fprintf('\rNegative tradeImb: \r'); 
get_stats(negTradeImb);
disp(['occurrence: ',num2str(length(negTradeImb)/length(tradeImb))])


