function [ upRight, upNum, downRight, downNum ] = impulseResponse( signal, price )
%IMPULSERESPONSE Summary of this function goes here
%   Detailed explanation goes here


% impulse response on tradeImb
diffMid = [diff(price); 0];

upSignal = signal > 0 ;
downSignal = signal < 0;

%disp(['number of up signal: ', num2str(sum(upSignal))]);
%disp(['number of down signal: ', num2str(sum(downSignal))]);

upSignal_next = [false; upSignal(1:end-1)]; % position +1
downSignal_next = [false; downSignal(1:end-1)];


upSignal_IR = sign(price(upSignal_next) - price(upSignal));
downSignal_IR = sign(price(downSignal_next) - price(downSignal));

upRight = sum(upSignal_IR>0) / sum(upSignal);
upNum = sum(upSignal);
downRight = sum(downSignal_IR<0) / sum(downSignal);
downNum = sum(downSignal);
%disp('up signal impulse response: ');
%tabulate(upSignal_IR)
%disp('down signal impulse response: ');
%tabulate(downSignal_IR)
end

