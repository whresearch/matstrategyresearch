function  get_imbSignal_profile(Data, tradeImbThreshold, bidRatioThreshold)

tickSize = Data.Contract.TickSize;

bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;
last = Data.Last;

bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

% trade imbalance
tradeImb = sizeBuy - sizeSell;
tradeImb(isnan(tradeImb))=0;

% signal impulse response

upSignal = tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold ;
downSignal = tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold ;



upSignal_next = [false; upSignal(1:end-1)]; % position +1
downSignal_next = [false; downSignal(1:end-1)];


upSignal_IR = mid(upSignal_next) - mid(upSignal);
downSignal_IR = mid(downSignal_next) - mid(downSignal);


upSignal_askSize = askSize(upSignal);
upSignal_bidSize = bidSize(upSignal);
upSignal_tradeImb = tradeImb(upSignal);

downSignal_askSize = askSize(downSignal);
downSignal_bidSize = bidSize(downSignal);
downSignal_tradeImb = tradeImb(downSignal);



dailyChange = last(end)/last(1) - 1; 
dailyChange_inTicks = (last(end) - last(1))/tickSize;
% general info
disp(['daily change: ', num2str(dailyChange), ' = ', num2str(dailyChange_inTicks), ' ticks']);

dailyRange = (max(last) - min(last))/tickSize;

disp(['daily range: ', num2str(dailyRange), ' ticks']);

fprintf('\rbidSize: \r'); 
get_stats(bidSize)

fprintf('\raskSize: \r'); 
get_stats(askSize)

% tradeImb info
posTradeImb = tradeImb(tradeImb > 0);
fprintf('\rPositive tradeImb: \r'); 
get_stats(posTradeImb);
disp(['occurrence: ',num2str(length(posTradeImb)/length(tradeImb))])

negTradeImb = tradeImb(tradeImb < 0);
fprintf('\rNegative tradeImb: \r'); 
get_stats(negTradeImb);
disp(['occurrence: ',num2str(length(negTradeImb)/length(tradeImb))])


% signal info 
fprintf('\r'); 
disp(['number of up signal: ', num2str(sum(upSignal))]);
disp('up signal impulse response: ');
tabulate(upSignal_IR)

fprintf('\r'); 
disp(['number of down signal: ', num2str(sum(downSignal))]);
disp('down signal impulse response: ');
tabulate(downSignal_IR)


fprintf('\r'); 
disp('up signal bidSize: ')
get_stats(upSignal_bidSize);

fprintf('\r'); 
disp('up signal askSize: ')
get_stats(upSignal_askSize);

fprintf('\r'); 
disp('up signal tradeImb: ')
get_stats(upSignal_tradeImb);

fprintf('\r'); 
disp('down signal bidSize: ')
get_stats(downSignal_bidSize);

fprintf('\r'); 
disp('down signal askSize: ')
get_stats(downSignal_askSize);

fprintf('\r'); 
disp('down signal tradeImb: ')
get_stats(downSignal_tradeImb);



end

