function  get_stats( v )

stats_mean = mean(v);
stats_std = std(v);
stats_median = median(v);
quantile_5 = quantile(v,0.05);
quantile_95 = quantile(v,0.95);


disp(['mean: ',num2str(stats_mean)]);

disp(['median: ',num2str(stats_median)]);

disp(['std: ',num2str(stats_std)]);

disp(['5% quantile: ',num2str(quantile_5)]);

disp(['95% quantile: ',num2str(quantile_95)]);


end

