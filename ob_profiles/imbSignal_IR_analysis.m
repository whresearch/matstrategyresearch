clc
clear

dataNum=1;

tradeImbThreshold = 650; % 40 for CU; 650 for rRB
bidRatioThreshold = 0.85;

%Data = LoadMat_SHFE('shfe_20160122_rb1605.mat');
Data = LoadMat_SHFE_RB_Main(dataNum); 

%contact information 
disp([upper(Data.Contract.Exchange),' ', datestr(Data.DayInfo.Day), ' ', upper(Data.Contract.Symbol)]);


tickSize = Data.Contract.TickSize;

% clear dataNum

%Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;

bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

% trade imbalance
tradeImb = sizeBuy - sizeSell;
tradeImb(isnan(tradeImb))=0;
cumTradeImb = cumsum(tradeImb);

[tradePx, tradeSize, ~] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec (bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);
    
passiveLiquidity = passiveOrderBid - passiveOrderAsk;

passiveLiquidity(isnan(passiveLiquidity)) = 0;

cumPL = cumsum(passiveLiquidity);

subplot(2,2,1)
plotyy(1:length(mid),cumTradeImb,1:length(mid),mid);
title('tradeImb vs mid')

subplot(2,2,2)
plotyy(1:length(cumPL), cumPL, 1:length(cumPL), cumTradeImb);
title('passiveLiquidity vs tradeImb')

totalLiquidity = passiveLiquidity + tradeImb ;

s_liquidity =  tradeImb - passiveLiquidity;

cum_s_liquidity = cumsum(s_liquidity);

cumTL = cumsum(totalLiquidity);

subplot(2,2,3)
plotyy(1:length(cumPL), cumTL, 1:length(cumPL), mid);
title('passiveLiquidity vs mid')

subplot(2,2,4)
plotyy(1:length(mid), cum_s_liquidity,1:length(mid), mid);
title('tradeImb-PL vs mid')


% impulse response on tradeImb
diffMid = [diff(mid); 0];

upSignal = tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold ;
downSignal = tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold ;

disp(['number of up signal: ', num2str(sum(upSignal))]);
disp(['number of down signal: ', num2str(sum(downSignal))]);

upSignal_next = [false; upSignal(1:end-1)]; % position +1
downSignal_next = [false; downSignal(1:end-1)];


upSignal_IR = mid(upSignal_next) - mid(upSignal);
downSignal_IR = mid(downSignal_next) - mid(downSignal);

disp('up signal impulse response: ');
tabulate(upSignal_IR)
disp('down signal impulse response: ');
tabulate(downSignal_IR)

true_upSignal = tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold & diffMid >= 1 * tickSize;
false_upSignal = tradeImb > tradeImbThreshold & bidRatio > bidRatioThreshold & diffMid < 1 * tickSize;

true_downSignal = tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold & diffMid <= -1 * tickSize;
false_downSignal = tradeImb < -1 * tradeImbThreshold & bidRatio < 1 - bidRatioThreshold & diffMid > -1 * tickSize;

true_upSignal_idx = find(true_upSignal > 0);
false_upSignal_idx = find(false_upSignal > 0);

true_downSignal_idx = find(true_downSignal > 0);
false_downSignal_idx = find(false_downSignal > 0);


upSignal_askSize = askSize(upSignal);
upSignal_bidSize = bidSize(upSignal);
upSignal_tradeImb = tradeImb(upSignal);

downSignal_askSize = askSize(downSignal);
downSignal_bidSize = bidSize(downSignal);
downSignal_tradeImb = tradeImb(downSignal);


dataTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, bidRatio, tradeImb, passiveLiquidity, upSignal, downSignal, cumTradeImb, VWAP);

%v_corr = corrcoef(sign(tradeImb), sign(passiveLiquidity));

%signInfo = sign(tradeImb .* passiveLiquidity);


% regression analysis on size and IR
x = [ones(length(upSignal_askSize),1) upSignal_askSize];
y = upSignal_IR;

b = x\y;

signals = upSignal - downSignal;

signal_seq = signals(signals~=0);

f_signalSeqAnalysis( signal_seq )





