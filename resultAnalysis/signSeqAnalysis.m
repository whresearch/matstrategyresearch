function [ num_paris, num_c, num_r, z ] = signSeqAnalysis( x )

    n = length(x);    
    
    num_paris = n - 1;
    num_c = 0;
    num_r = 0;
    num_unknown = 0;
    
    for i = 1 : n -1 
        if x(i) * x(i+1) > 0
            num_c = num_c + 1;
        elseif x(i) * x(i+1) < 0
            num_r = num_r + 1;
        else
            num_unknown = num_unknown + 1;
    
        end
        
    end
    
    x = num_c + 0.5 * num_unknown;
    
    z = 2*(x-(n-1)/2)/sqrt(n);

end

