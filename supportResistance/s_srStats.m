Data = LoadMat_SHFE('shfe_20160108_cu1603.mat');

time = Data.Time;
bidSz = Data.BidSize;
bidPx = Data.Bid;
askPx = Data.Ask;
mid = Data.Mid;
askSz = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
vwap = Data.VWAP;
tickSize = 10;

lbw = 30;


vwap(volume==0)=nan;

[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPx, askPx, bidSz, askSz, vwap, volume, tickSize );

minPrice = min(min(askPx), min(bidPx));
maxPrice = max(max(askPx), max(bidPx));
len = (maxPrice-minPrice)/tickSize+1;
tradeVolByPrice = zeros (len,1);

supportResistance = zeros(size(tradePx,1),2);

for i=1:size(tradePx,1)
    px=tradePx(i,:);
    sz=tradeSize(i,:);
    cond=~isnan(px);
    tradePxIdx = (px-minPrice)/tickSize+1;
    idx=tradePxIdx(cond);
    sz2=sz(cond);
    newsz = tradeVolByPrice(idx) + sz2';
    tradeVolByPrice(idx) = newsz;

    
    askPxIdx = (askPx(i)-minPrice)/tickSize+1;
    bidPxIdx = (bidPx(i)-minPrice)/tickSize+1;
    
    [~,supportPxIdx] = max(tradeVolByPrice(1:bidPxIdx));
    [~,resistancePxIdx] = max(tradeVolByPrice(askPxIdx:end));
    resistancePxIdx=resistancePxIdx+askPxIdx-1;
    
    srPxIdx = [supportPxIdx, resistancePxIdx];
    srPx = (srPxIdx-1)*tickSize+minPrice;  
    
    supportResistance(i,:)=srPx;
end



