clear
close all

%fileList
contract1 = LoadMat_Range_Multiple( 'shfe', 'cu1509', datetime('2015-05-04'),  datetime('2015-10-26'));
contract2 = LoadMat_Range_Multiple( 'shfe', 'cu1510', datetime('2015-05-04'),  datetime('2015-10-26'));

joined = innerjoin (contract1, contract2, 'Keys', 'Date');

deliveredContract1 = contract1.Data(end).OpenInterestAbs(end);
deliveredContract2 = contract2.Data(end).OpenInterestAbs(end);

%ratio is speculator vs commodity user
deliveredRatioContract1 = deliveredContract1/max(contract1.Data(end).OpenInterestAbs);
deliveredRatioContract2 = deliveredContract2/max(contract1.Data(end).OpenInterestAbs);


%plot (joined{1,'Data_contract1'}.OpenInterestAbs);
%plot (joined{1,'Data_contract2'}.OpenInterestAbs);

%tmp1={joined.Data_contract1};
%tmp = getfield(joined.Data_contract1, 'OpenInterestAbs');

n = size(joined, 1);

for k = 1:n
    disp (joined.Date(k));
    time = joined.Data_contract1(k).SecondOfDay;
    tmp = joined.Data_contract1(k).OpenInterestAbs;
    vol = joined.Data_contract1(k).VolumeAbs;
    c1DayChange(k) = tmp(end) - tmp(10);
    c1_1(k) = tmp(10);
    %[~,~,v] = find(tmp, 1, 'first');
    %c1_1(k) = v;
    [open, close1, high, low, openPos, closePos] = openInterestStatsByTime (1800, 75600, time, tmp, vol);
    c1_hourlydiff(k,:) = (close1-open)';%./c1DayChange(k);
    
    c1_openPos(k,:) = openPos';
    c1_closePos(k,:) = closePos';

    
    time = joined.Data_contract2(k).SecondOfDay;
    tmp = joined.Data_contract2(k).OpenInterestAbs;
    vol = joined.Data_contract2(k).VolumeAbs;
    c2DayChange(k) = tmp(end) - tmp(10);
    c2_1(k) = tmp(10);
    %[~,~,v] = find(tmp, 1, 'first');
    %c2_1(k) = v;
    [open, close1, high, low] = openInterestStatsByTime (1800, 75600, time, tmp, vol);
    c2_hourlydiff(k,:) = (close1-open)';%./c2DayChange(k);
    c2_openPos(k,:) = openPos';
    c2_closePos(k,:) = closePos';
    
    c1_end(k) = joined.Data_contract1(k).OpenInterestAbs(end);
    c2_end(k) = joined.Data_contract2(k).OpenInterestAbs(end);
end

c1MinusPriorDayMktOpen = diff(c1_1);
c2MinusPriorDayMktOpen = diff(c2_1);

c1MinusPriorDayMktClose = c1_1(2:end) - c1_end(1:end-1);
c2MinusPriorDayMktClose = c2_1(2:end) - c2_end(1:end-1);

c1MktCloseOiChgRatio = c1MinusPriorDayMktClose./c1MinusPriorDayMktOpen;
c2MktCloseOiChgRatio = c2MinusPriorDayMktClose./c2MinusPriorDayMktOpen;

figure
plot (c1_1);
legend('contract1 a few secs after market open');
figure
plot (c2_1);
legend('contract2 a few secs after market open');
figure
plot (c1MktCloseOiChgRatio);
legend('contract1 chg from prev day. close+auction %');
figure
plot (c2MktCloseOiChgRatio);
legend('contract2 chg from prev day. close+auction %');
%figure
%plot (oiEnd)





