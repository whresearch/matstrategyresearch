function [ open, close1, high, low, openPos, closePos ] = openInterestStatsByTime( binLen, startOfDay, time, oi, vol )
%OPENINTERESTSTATSBYTIME Summary of this function goes here
%   Detailed explanation goes here
    numBins = int64(24*3600 / binLen);

    open = zeros (numBins, 1);
    close1 = zeros (numBins, 1);
    high = zeros (numBins, 1);
    low = zeros (numBins, 1);
    openPos = zeros (numBins, 1);
    closePos = zeros (numBins, 1);
    
    [ openIdx, closeIdx, binIdx ] = binByTime( binLen, startOfDay, time, oi );
    
    

    lastTickIdx = length(oi);
    
    for i = 1:length(openIdx)
        oidx = openIdx(i);
        cidx = closeIdx(i);
        segment = oi (oidx:cidx);
        
        open(binIdx(i)) = oi(oidx);
        close1(binIdx(i)) = oi(cidx);
    
        high(binIdx(i)) = max(segment);
        low(binIdx(i)) = min(segment);
        
        cidxNext = min (lastTickIdx, cidx);
        
        oiDelta = oi(cidxNext) - oi(oidx);
        volDelta = double(vol(cidxNext) - vol (oidx));
        
        openPos(binIdx(i)) = (volDelta + oiDelta)/2;
        closePos(binIdx(i)) = (volDelta -oiDelta)/2;
        
        
    end  
end

