function [ openIdx, closeIdx, binIdx ] = binByTime( binLen, startOfDay, time, value )
%BINBYTIME Summary of this function goes here
%   Detailed explanation goes here

    t = time-startOfDay;
    t2 = idivide (int64(t), int64(binLen))+1;

    u = unique(t2);
    
    numUniqueTimeBins = length (u);
    openIdx = zeros(numUniqueTimeBins, 1);
    closeIdx = zeros(numUniqueTimeBins, 1);
    binIdx = u;

    for i = 1:numUniqueTimeBins
        openIdx(i) = find (t2==binIdx(i), 1, 'first');
        closeIdx(i) = find (t2==binIdx(i), 1, 'last');
    end

end

