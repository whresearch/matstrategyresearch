clc
clear
close all
tic
%figure
start_day = 110;
num_days = 25;
maxLaw = 150;
timeFilter = [0.05; 0.05; 0.1; 0.8];

threshold = 30;

avg_increment_mat = zeros(maxLaw,num_days);

for dataNum = start_day:start_day+num_days-1


Data = LoadMat_SHFE_CU_Main(dataNum); 

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
lastPrice = Data.Last;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;


[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

buySellDiff = sizeBuy - sizeSell ;

% upper_quantile = quantile(buySellDiff,0.85);
% 
% lower_quantile = quantile(buySellDiff,0.15);

%upper quantile and lower quantile seems to be very symmetric 

cumBuySell = cumsum(buySellDiff);

%plotyy(1:length(mid),mid,1:length(mid),cumBuySell);

% acf = autocorr(buySellDiff); 

lbw = 4;

signal =  createRollingWindow(buySellDiff, lbw) * timeFilter;



avg_increment = zeros(maxLaw,1);
    
%condition = signal > threshold;
condition = signal < - threshold;
    
[ impactMean, impactWinSelected ] = priceImpact( condition, mid(lbw:end), maxLaw );
  

avg_increment_mat(:,dataNum-start_day+1) = impactMean';

%plot(impactMean);

%hold on

end


avg_increment_vec = mean(avg_increment_mat,2);

plot(avg_increment_vec);

toc






