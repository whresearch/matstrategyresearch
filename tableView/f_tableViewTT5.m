function [ out_table ] = f_tableViewTT5( data, trades, pnl, cumPnl)

    Data = data;
    time = Data.Time;
    bidSize = Data.BidSize;
    bidPrice = Data.Bid;
    askPrice = Data.Ask;

    askSize = Data.AskSize;
    volume = Data.Volume;

    openVol = Data.OpenVolume;
    closeVol = Data.CloseVolume;
    VWAP = Data.VWAP;
    tickSize = 10;

    n = size(askPrice,1);
    
    if nargin < 2
        trades = zeros(n,1);
        pnl    = zeros(n,1);
        cumPnl = zeros(n,1);
    end
    %openRatio = Data.OpenVolume./Data.Volume;

    askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
    bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

    [sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);


    sizeBuyInt=int32(sizeBuy);
    sizeSellInt=int32(sizeSell);

    tradeImb = sizeBuyInt - sizeSellInt;

    signal = zeros(n,1);




    out_table = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, bidRatio, VWAP, tradeImb, trades, pnl, cumPnl);

end

