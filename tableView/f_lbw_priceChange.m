function [ lbw_priceChange ] = f_lbw_priceChange( mid, tickSize)

n = length(mid);
lbw_priceChange = zeros(n,1);

ma3_mid = filter([1/3 1/3 1/3], 1, mid);

lbw_priceChange(4:n) = abs(ma3_mid(3:n-1) - mid(4:n)) > 0.5 * tickSize;

end

