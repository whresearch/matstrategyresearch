function [ feature_table, feature_mat ] = f_featureTable( Data, trades, pnl )

    time = Data.Time;
    bidSize = Data.BidSize;
    bidPrice = Data.Bid;
    askPrice = Data.Ask;
    mid = Data.Mid;
    askSize = Data.AskSize;
    volume = Data.Volume;
    %openInterest = Data.OpenInterest;
    %cumsumOpenInterest = cumsum (openInterest);
    %openVol = Data.OpenVolume;
    %closeVol = Data.CloseVolume;
    VWAP = Data.VWAP;
    tickSize = Data.Contract.TickSize;

    n = size(askPrice,1);
    

    %openRatio = Data.OpenVolume./Data.Volume;

%     askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
    bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

    [sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

%     [open, close] = openCloseByTrade (volume, openVol, closeVol, sizeBuy, sizeSell);

    tradeImb = sizeBuy - sizeSell;
    
%     openImb = open(:,1)./(open(:,1) + open(:,2));
%     closeImb = close(:,1)./(close(:,1) + close(:,2));

% features: lbw_avg_pl, pl, lbw_priceChange, bidRatio, tradeImb, feature_AD, largeSpread, shortSideSize,   
    
    [tradePx, tradeSize, ~ ] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);
    
    [passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec(bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);
    
    passiveLiquidity = passiveOrderBid - passiveOrderAsk;
    
    lbw8_pl =  filter([1/8 1/8 1/8 1/8 1/8 1/8 1/8 1/8], 1, passiveLiquidity);
    
    lbw_priceChange = f_lbw_priceChange( mid, tickSize );
    
    largeSpread = askPrice - bidPrice > tickSize;
    
    shortSideSize = min(askSize, bidSize);
    
    trade_idx = find(~isnan(trades));
    
    enterTrade_idx = trade_idx(1:2:length(trade_idx));
    
    feature_table = table(time, lbw8_pl, passiveLiquidity, lbw_priceChange, bidRatio, tradeImb, largeSpread, shortSideSize, trades);
    
    feature_table = feature_table(enterTrade_idx,:);
    
    pnl = pnl(~isnan(pnl));
    
    feature_table = [feature_table, table(pnl)];
    
    feature_mat = [feature_table.lbw8_pl, feature_table.passiveLiquidity, feature_table.lbw_priceChange, feature_table.bidRatio, feature_table.tradeImb, feature_table.largeSpread, feature_table.shortSideSize, feature_table.trades, pnl];
    
end

