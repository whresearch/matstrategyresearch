clear

dataNum=135;

Data = LoadMat_SHFE_CU_Main(dataNum); clear dataNum

% Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;

lbw = 30;

mid_mkt1 = zeros(Data.Length,1);
mid_mkt2= zeros(Data.Length,1);

for i = lbw : Data.Length
    
    [mid_mkt1(i), mid_mkt2(i)] = MKT2(Data.Mid(i-lbw+1:i));

end

openRatio = Data.OpenVolume./Data.Volume;
askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);



%[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[open, close] = openCloseByTrade (volume, openVol, closeVol, sizeBuy, sizeSell);

sizeBuyInt=int32(sizeBuy);
sizeSellInt=int32(sizeSell);


ordImb = sizeBuy./(sizeBuy+sizeSell);
openImb = open(:,1)./(open(:,1) + open(:,2));
closeImb = close(:,1)./(close(:,1) + close(:,2));

dataTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol,askRatio, openRatio, sizeBuyInt, sizeSellInt,VWAP);

signalTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, mid_mkt1, mid_mkt2,askRatio,openRatio, sizeBuyInt, sizeSellInt,VWAP);

openInterestTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, askRatio,openRatio, openInterest, cumsumOpenInterest, open, close, sizeBuyInt, sizeSellInt,VWAP);


figure
hold on
[ impactMean, impactWinSelected ] = priceImpact(ordImb<0.3 & (sizeBuy+sizeSell)>20, mid, 100);
plot(impactMean)
[ impactMean, impactWinSelected ] = priceImpact(openImb<0.3 & (open(:,1) + open(:,2))>5, mid, 100);
plot(impactMean)
[ impactMean, impactWinSelected ] = priceImpact(closeImb<0.3 & (close(:,1) + close(:,2))>5, mid, 100);
plot(impactMean)

figure
hold on
[ impactMean, impactWinSelected ] = priceImpact(ordImb>0.7 & (sizeBuy+sizeSell)>20, mid, 100);
plot(impactMean)
[ impactMean, impactWinSelected ] = priceImpact(openImb>0.7 & (open(:,1) + open(:,2))>5, mid, 100);
plot(impactMean)
[ impactMean, impactWinSelected ] = priceImpact(closeImb>0.7 & (close(:,1) + close(:,2))>5, mid, 100);
plot(impactMean)


% figure
% hold on
% [ impactMean, impactWinSelected ] = priceImpact(sizeSell>20, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(open(:,2)>5, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(close(:,2)>5, mid, 100);
% plot(impactMean)


