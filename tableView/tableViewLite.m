clc
clear
Data = LoadMat_SHFE('shfe_20151215_cu1602.mat');
time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;

n = size(askPrice,1);

%openRatio = Data.OpenVolume./Data.Volume;

askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[open, close] = openCloseByTrade (volume, openVol, closeVol, sizeBuy, sizeSell);

sizeBuyInt=int32(sizeBuy);
sizeSellInt=int32(sizeSell);

tradeImb = sizeBuy - sizeSell;
openImb = open(:,1)./(open(:,1) + open(:,2));
closeImb = close(:,1)./(close(:,1) + close(:,2));

signal = zeros(n,1);

tradeImbThreshold = 85;
bidRatioThreshold = 0.85;

for i = 1 : n
    
    if tradeImb(i) > tradeImbThreshold && bidRatio(i) > bidRatioThreshold
        %disp('signal is 1 now');
        signal(i) = 1;
    elseif tradeImb(i) < -tradeImbThreshold && askRatio(i) > bidRatioThreshold
        %disp('signal is -1 now');
        signal(i) = -1;
    end
end

dataTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, bidRatio,tradeImb,signal );
