function [ out_table ] = f_tradeTable( data, trades, pnl, cumPnl, signal )

    Data = data;
    time = Data.Time;
    bidSize = Data.BidSize;
    bidPrice = Data.Bid;
    askPrice = Data.Ask;
    %mid = Data.Mid;
    askSize = Data.AskSize;
    volume = Data.Volume;
    %openInterest = Data.OpenInterest;
    %cumsumOpenInterest = cumsum (openInterest);
    openVol = Data.OpenVolume;
    %closeVol = Data.CloseVolume;
    VWAP = Data.VWAP;
    tickSize = data.Contract.TickSize;

    n = size(askPrice,1);
    
    if nargin < 2
        trades = zeros(n,1);
        pnl    = zeros(n,1);
        cumPnl = zeros(n,1);
    end
    %openRatio = Data.OpenVolume./Data.Volume;

%     askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
    bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

    [sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

%     [open, close] = openCloseByTrade (volume, openVol, closeVol, sizeBuy, sizeSell);

    sizeBuyInt=int32(sizeBuy);
    sizeSellInt=int32(sizeSell);

    tradeImb = sizeBuyInt - sizeSellInt;
%     openImb = open(:,1)./(open(:,1) + open(:,2));
%     closeImb = close(:,1)./(close(:,1) + close(:,2));

    
    [tradePx, tradeSize, ~ ] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);
    
    [passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec(bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);
    
    passiveLiquidity = passiveOrderBid - passiveOrderAsk;
    
    trade_idx = find(~isnan(trades));
    
    enterTrade_idx = trade_idx(1:2:length(trade_idx));
    
    out_table = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, bidRatio, VWAP, tradeImb, passiveLiquidity, signal, trades);
    
    out_table = out_table(enterTrade_idx,:);

    pnl = pnl(~isnan(pnl));
    
    cumPnl = cumsum(pnl);
    
    out_table = [out_table, table(pnl), table(cumPnl)];
    
end

