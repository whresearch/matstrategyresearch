clc
clear

dataNum=96;

%Data = LoadMat_SHFE_CU_Main(dataNum); 

Data = LoadMat_SHFE('shfe_20160111_cu1603.mat'); 

clear dataNum


%Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;


openRatio = Data.OpenVolume./Data.Volume;
askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);


%[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

[open, close] = openCloseByTrade (volume, openVol, closeVol, sizeBuy, sizeSell);

sizeBuyInt=int32(sizeBuy);
sizeSellInt=int32(sizeSell);


ordImb = sizeBuy./(sizeBuy+sizeSell);


tradeImb = sizeBuy - sizeSell;


openImb = open(:,1)./(open(:,1) + open(:,2));
closeImb = close(:,1)./(close(:,1) + close(:,2));

dataTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol,askRatio, openRatio, sizeBuyInt, sizeSellInt,VWAP);

openInterestTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol, askRatio,openRatio, openInterest, cumsumOpenInterest, open, close, sizeBuyInt, sizeSellInt,VWAP);
tradeImbTable = table(time, bidSize, bidPrice, askPrice, askSize, volume, openVol,askRatio, openRatio, tradeImb, sizeBuyInt, sizeSellInt,VWAP);

figure
hold on
[ impactMean, impactWinSelected ] = priceImpact(bidRatio > 0.85 & tradeImb > 40, mid, 100);
plot(impactMean)

% [ impactMean, impactWinSelected ] = priceImpact(openImb<0.3 & (open(:,1) + open(:,2))>5, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(closeImb<0.3 & (close(:,1) + close(:,2))>5, mid, 100);
% plot(impactMean)

figure
hold on
[ impactMean, impactWinSelected ] = priceImpact(askRatio > 0.85 & tradeImb < -40, mid, 100);
plot(impactMean)

% [ impactMean, impactWinSelected ] = priceImpact(openImb>0.7 & (open(:,1) + open(:,2))>5, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(closeImb>0.7 & (close(:,1) + close(:,2))>5, mid, 100);
% plot(impactMean)


% figure
% hold on
% [ impactMean, impactWinSelected ] = priceImpact(sizeSell>20, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(open(:,2)>5, mid, 100);
% plot(impactMean)
% [ impactMean, impactWinSelected ] = priceImpact(close(:,2)>5, mid, 100);
% plot(impactMean)


