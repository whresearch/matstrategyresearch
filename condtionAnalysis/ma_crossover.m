clc
clear

%dataNum=96;

%Data = LoadMat_SHFE_CU_Main(dataNum); 

Data = LoadMat_SHFE('shfe_20160108_cu1603.mat'); 

clear dataNum


%Data.FilterByHours([21 0],[24 1]);

time = Data.Time;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
openInterest = Data.OpenInterest;
cumsumOpenInterest = cumsum (openInterest);
openVol = Data.OpenVolume;
closeVol = Data.CloseVolume;
VWAP = Data.VWAP;
tickSize = 10;


openRatio = Data.OpenVolume./Data.Volume;
askRatio = Data.AskSize./(Data.AskSize + Data.BidSize);
bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

[sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

tradeImb = sizeBuy - sizeSell;

lbw1 = 50;
lbw2 = 100;
a = 1;

b1 = ones(1,lbw1) *1/lbw1;
b2 = ones(1,lbw2) *1/lbw2;

ma_short = filter(b1,a,tradeImb);

ma_long  = filter(b2,a,tradeImb);

condition = zeros(length(ma_short),1);

for i = 1 : length(condition) - 1
   if ma_short(i) < ma_long(i) && ma_short(i+1) >= ma_long(i+1)
    condition(i+1) = 1;
       
   end    
    
end

figure
hold on

[ impactMean, impactWinSelected ] = priceImpact(condition, mid, 100);

plot(impactMean)


