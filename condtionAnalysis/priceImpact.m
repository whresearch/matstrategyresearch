function [ impactMean, impactWinSelected ] = priceImpact( cond, mid, lookFwd )

    disp(['Num of instance: ', num2str(sum(cond))]);
    
    midWin = createRollingWindow (mid, lookFwd);
    
    baseWin = repmat (midWin(:,1), 1, size(midWin,2));
    
    impactWin = midWin-baseWin;

    condResized  = cond (1:size(midWin,1));
    
    %impactWinDropCond = impactWin (:, 2) < 0;
    
    impactWinCond = condResized;% & impactWinDropCond;
    
    impactWinSelected = impactWin (impactWinCond, :);
    
    impactMean = mean (impactWinSelected, 1);
    %impactMean = median (impactWinSelected, 1);
end

