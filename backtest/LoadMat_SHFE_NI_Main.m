function [ Output ] = LoadMat_SHFE_NI_Main( Index )
% LoadMat_SHFE_Main loads a single copper main contract mat data with given index

% load main contract list
List = load('NI_MainContract_List.mat');

% load data
Output = LoadMat_SHFE(List.Contracts{Index});

end

