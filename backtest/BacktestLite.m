classdef BacktestLite  < handle
    %BACKTESTLITE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        tickSize
        
        HistoData
        Strategy
        Time
        Bid
        Ask
        BidSize
        AskSize
        Mid
        SWMid
        OpenInterest
        OpenVolume
        Volume
        VWAP
        
        vwapPxVec
        vwapSzVec
        vwapSideVec
        
        vwapPx
        vwapSz
        vwapSide
        
        curTick
        curOrder
        curPos
        curTime
        snapshot
        
        curTrade
        lastTrade
        
        actions
        orders
        trades
        pnl
        pnlVec

        numCancelled
        sharpe
    end
    
    methods
        
        function this = BacktestLite( HistoData, Strategy)
            warning ('off', 'backtrace');
            % constructor
            this.tickSize = Strategy.tickSize;
            this.HistoData = HistoData;
            this.Time = HistoData.Time;
            this.Bid = HistoData.Bid;
            this.Ask = HistoData.Ask;
            this.BidSize = HistoData.BidSize;
            this.AskSize = HistoData.AskSize;
            this.Mid = HistoData.Mid;
            this.SWMid = HistoData.SWMid;
            this.OpenInterest = HistoData.OpenInterest;
            this.OpenVolume = HistoData.OpenVolume;
            this.Volume = HistoData.Volume;
            this.VWAP = HistoData.VWAP;
            this.Strategy = Strategy;
            this.Strategy.RegisterWith(this);
            
            this.snapshot = Snapshot;
                        
            [this.vwapPxVec, this.vwapSzVec, this.vwapSideVec] = tradeVolByVwap(this.Bid, this.Ask, this.BidSize, this.AskSize, this.VWAP, this.Volume, this.tickSize );
            
            this.curOrder = OrderLite;
            this.curOrder.Amount = 0;
            this.curPos = 0;
            
            this.curTime = datetime;
            this.pnl=0;
            this.sharpe=0;
            this.numCancelled = 0; 
            
        end
        
        
        function run(this)
            for i = 1:this.HistoData.Length
                
                %snapshot = this.HistoData.GetRow (i);
                %display(snapshot);
                this.curTick = i;
                this.curTime = this.Time(i);
                this.snapshot.Time = this.Time(i);
                %this.snapshot.Tick = i;
                %snapshot.Last = this.Last(snapshotIdx);
                this.snapshot.Bid = this.Bid(i);
                this.snapshot.Ask = this.Ask(i);
                this.snapshot.Mid = this.Mid(i);
                this.snapshot.SWMid = this.SWMid(i);
                this.snapshot.BidSize = this.BidSize(i);
                this.snapshot.AskSize = this.AskSize(i);
                this.snapshot.OpenInterest = this.OpenInterest(i);
                this.snapshot.Volume = this.Volume(i);
                this.snapshot.VWAP = this.VWAP(i);
                this.snapshot.OpenVolume = this.OpenVolume(i);
                
                %snapshot.CloseVolume = this.CloseVolume(snapshotIdx);
                %snapshot.Turnover = this.Turnover(snapshotIdx);
                %snapshot.VWAP = this.VWAP(snapshotIdx);
                this.vwapPx = this.vwapPxVec(i,:);
                this.vwapSz = this.vwapSzVec(i,:);
                this.vwapSide = this.vwapSideVec(i,:);
                
                this.checkTrades();
                this.Strategy.OnMarketDataUpdate (' ', this.snapshot);
                %this.Strategy.OnMarketDataUpdate2 (' ', this.Bid(i), this.Ask(i), this.BidSize(i), this.AskSize(i), this.Mid(i), this.OpenVolume(i), this.Volume(i), i);                
            end
            
%             for i = 1:length(this.orders)
%                 this.orders(i)
%             end
%             for i = 1:length(this.trades)
%                 this.trades(i)
%             end
            %for i = 1:length(this.actions)
                
                %disp(['Action:', this.actions(i)]);
            %end
            this.pnl = this.pnl - 0.2 * this.tickSize * length(this.pnlVec);
            this.pnlVec = this.pnlVec - 0.2 * this.tickSize;
        end
        
        function showResults (this)
            
            %this.actions
            tabulate (this.pnlVec);
            disp(['num trades: ',num2str(length(this.pnlVec))]);
            
            avgPnl = mean(this.pnlVec);
            disp(['avg pnl: ',num2str(avgPnl)]);
            
            winRatio = sum(this.pnlVec>0)/length(this.pnlVec);
            disp(['win ratio: ',num2str(winRatio)]);
            
            avgWin = mean(this.pnlVec(this.pnlVec>0)); 
            disp(['avg win: ',num2str(avgWin)]);
            
            evenRatio = sum(this.pnlVec==0)/length(this.pnlVec);
            disp(['even ratio: ',num2str(evenRatio)]);
            
            loseRatio = sum(this.pnlVec<0)/length(this.pnlVec);
            disp(['lose ratio: ',num2str(loseRatio)]);
            
            avgLose = mean(this.pnlVec(this.pnlVec<0)); 
            disp(['avg lose: ',num2str(avgLose)]);
            
            disp(['total pnl: ',num2str(this.pnl)]);
            
%             this.sharpe = this.pnl/std(this.pnlVec);
%             disp(['sharpe ratio: ',num2str(this.sharpe)]);
            
            disp(['number of orders cancelled: ',num2str(this.numCancelled)]);

        end
        
        
        function BuyOpen( this, InstrumentID, Amount, Price, IsIOC )
            if (this.curOrder.Amount ~=0)
                error('Error already has open order');
            else                 
                this.curOrder.Amount = Amount;
                this.curOrder.Price = Price;
                this.curOrder.IsLong = true;
                this.curOrder.IsOpen = true;
                this.curOrder.IsIOC = IsIOC;
                this.curOrder.QueuePos = this.snapshot.BidSize+1;
                
                action = ActionLite.fromOrder (this.curTick, {'BuyOpen'}, this.curOrder);
                this.actions = [this.actions;action];
                
                this.orders = [this.orders this.curOrder];
                
                this.Strategy.OnOrderInsertAck(this.curOrder);
                
                if (IsIOC)
                    if (this.curOrder.Price>=this.snapshot.Ask)
                        this.OnTrade (this.curOrder, this.curOrder.Amount, this.curOrder.Price);
                        this.curOrder.Amount = 0;
                    else
                        this.Strategy.OnOrderCancel (this.curOrder);
                        this.curOrder.Amount = 0;
                    end
                end
            end
        end
        
        function SellOpen( this, ~, Amount, Price, IsIOC )
            if (this.curOrder.Amount ~=0)
                error('Error already has open order');
            else
                this.curOrder.Amount = Amount;
                this.curOrder.Price = Price;
                this.curOrder.IsLong = false;
                this.curOrder.IsOpen = true;
                this.curOrder.IsIOC = IsIOC;
                this.curOrder.QueuePos = this.snapshot.AskSize+1;
                
                action = ActionLite.fromOrder (this.curTick, {'SellOpen'}, this.curOrder);
                this.actions = [this.actions;action];
                
                this.orders = [this.orders this.curOrder];
                
                this.Strategy.OnOrderInsertAck(this.curOrder);
                
                if (IsIOC)
                    if (this.curOrder.Price<=this.snapshot.Bid)
                        this.OnTrade (this.curOrder, this.curOrder.Amount, this.curOrder.Price);
                        this.curOrder.Amount = 0;
                    else
                        this.Strategy.OnOrderCancel (this.curOrder);
                        this.curOrder.Amount = 0;
                    end
                end
            end
        end
        
        function BuyClose( this, InstrumentID, Amount, Price, IsIOC )
            if (this.curOrder.Amount ~=0)
                error('Error already has open order');
            elseif (this.curPos >=0)
                error('Error current position >=0');
            else
                this.curOrder.Amount = Amount;
                this.curOrder.Price = Price;
                this.curOrder.IsLong = true;
                this.curOrder.IsOpen = false;
                this.curOrder.IsIOC = IsIOC;                
                this.curOrder.QueuePos = this.snapshot.BidSize+1;
                
                action = ActionLite.fromOrder (this.curTick, {'BuyClose'}, this.curOrder);
                this.actions = [this.actions;action];
                
                this.orders = [this.orders this.curOrder];
                
                this.Strategy.OnOrderInsertAck(this.curOrder);
                if (IsIOC)
                    if (this.curOrder.Price>=this.snapshot.Ask)
                        this.OnTrade (this.curOrder, this.curOrder.Amount, this.curOrder.Price);
                        this.curOrder.Amount = 0;
                    else
                        this.Strategy.OnOrderCancel (this.curOrder);
                        this.curOrder.Amount = 0;
                    end
                end                
            end
        end
        
        function SellClose( this, InstrumentID, Amount, Price, IsIOC )
            if (this.curOrder.Amount ~=0)
                error('Error already has open order');
            elseif (this.curPos <=0)
                error('Error current position <=0');
            else
                this.curOrder.Amount = Amount;
                this.curOrder.Price = Price;
                this.curOrder.IsLong = false;
                this.curOrder.IsOpen = false;
                this.curOrder.IsIOC = IsIOC;
                this.curOrder.QueuePos = this.snapshot.AskSize+1;
                
                action = ActionLite.fromOrder (this.curTick, {'SellClose'}, this.curOrder);
                this.actions = [this.actions;action];
                
                this.orders = [this.orders this.curOrder];
                
                this.Strategy.OnOrderInsertAck(this.curOrder);
                if (IsIOC)
                    if (this.curOrder.Price<=this.snapshot.Bid)
                        this.OnTrade (this.curOrder, this.curOrder.Amount, this.curOrder.Price);
                        this.curOrder.Amount = 0;
                    else
                        this.Strategy.OnOrderCancel (this.curOrder);
                        this.curOrder.Amount = 0;
                    end
                end                
            end
        end
        
        function CancelAllOrder(this)
            if (this.curOrder.Amount ==0)
                warning('warning no open order to cancel, carry on');
            else
                this.numCancelled = this.numCancelled + 1;
                action = ActionLite.fromOrder (this.curTick, {'CancelAllOrder'}, this.curOrder);
                this.actions = [this.actions;action];
                
                this.Strategy.OnOrderCancel (this.curOrder);
                
                this.curOrder.Amount = 0;
            end
        end
        
        
        function checkTrades(this)
            if (this.curOrder.Amount == 0)
                return
            end
            vwapIsBuy = (this.vwapSide(1) == 1);
            this.checkTradeByVwap (this.vwapPx(1), this.vwapSz(1), vwapIsBuy);
            vwapIsBuy = (this.vwapSide(2) == 1);
            this.checkTradeByVwap (this.vwapPx(2), this.vwapSz(2), vwapIsBuy);
        
        end
        
        function checkTradeByVwap (this, vwapPx, vwapSz, vwapIsBuy)
            if (vwapIsBuy == ~this.curOrder.IsLong && this.curOrder.Price == vwapPx)
                this.curOrder.QueuePos = this.curOrder.QueuePos - vwapSz;
                if (this.curOrder.QueuePos<=0)
                    tradedSz = 1-this.curOrder.QueuePos;
                    curOrdTradedSz = min(this.curOrder.Amount, tradedSz);
                    this.curOrder.Amount = this.curOrder.Amount - curOrdTradedSz;
                    this.OnTrade (this.curOrder, curOrdTradedSz, this.curOrder.Price);
                end
            end
        end
        
        function OnTrade( this, Order, Amount, Price )
            this.lastTrade = this.curTrade;
            this.curTrade = Trade(1, Order, this.curTime, Amount, Price);
            this.trades = [this.curTrade this.trades];

            action = ActionLite.fromTrade (this.curTick, {'Trade'}, this.curTrade);
            this.actions = [this.actions;action];
                
            this.Strategy.OnTrade (Order, Amount, Price);
            if (~Order.IsOpen)
                diffValue = (this.curTrade.Price - this.lastTrade.Price)*Amount;
                if (Order.IsLong) %because SellClose is closing a buy
                    diffValue = -diffValue;
                end
                this.pnl = this.pnl+diffValue;
                this.pnlVec = [this.pnlVec;diffValue];
                %disp(['PnL:', num2str(diffValue), '/', num2str(this.pnl)]);
                
                if (Order.IsLong)
                    this.curPos = this.curPos + Amount;
                else
                    this.curPos = this.curPos - Amount;
                end
                
            else
                if (Order.IsLong)
                    this.curPos = this.curPos + Amount;
                else
                    this.curPos = this.curPos - Amount;
                end
            end
        end
        
    end
    
end

