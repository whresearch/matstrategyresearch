classdef (Abstract) StrategyLite < handle
    %STRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)
        Backtest
        
    end
    
    
    properties 
        cumPnl = 0;
        pnl    = [];
        trades = [];
        signals = []; 
        
    end
    
    methods (Abstract)
        
        OnInit( this, InstrumentID, Contract, TradingDay )

        OnMarketDataUpdate( this, InstrumentID, snapshot)
        %OnMarketDataUpdate2( this, InstrumentID, Bid, Ask, BidSize, AskSize, Mid, OpenVolume, Volume, Time)
        
        OnOrderInsertAck( this, Order )
        
        OnOrderCancel( this, Order )
        
        OnTrade( this, Order, Amount, Price )
        
        ShowResults(this)
        
    end
    
    methods
        
        function BuyOpen( this, InstrumentID, Amount, Price, IsIOC )
            this.Backtest.BuyOpen( InstrumentID, Amount, Price, IsIOC );
        end
        
        function SellOpen( this, InstrumentID, Amount, Price, IsIOC )
            this.Backtest.SellOpen( InstrumentID, Amount, Price, IsIOC );
        end
        
        function BuyClose( this, InstrumentID, Amount, Price, IsIOC )
            this.Backtest.BuyClose( InstrumentID, Amount, Price, IsIOC );
        end
        
        function SellClose( this, InstrumentID, Amount, Price, IsIOC )
            this.Backtest.SellClose( InstrumentID, Amount, Price, IsIOC );
        end
        
        function CancelAllOrder(this)
            this.Backtest.CancelAllOrder();
        end
              
    end
    
    
    methods (Access = { ?BacktestLite })
        
        function RegisterWith( this, Backtest )
            this.Backtest = Backtest;
        end
        
    end
       
end



