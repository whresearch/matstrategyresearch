classdef ActionLite
    %ACTIONLITE Summary of obj class goes here
    %   Detailed explanation goes here
    
    properties
        tick
        action
        isBuy
        isOpen
        price
        amount
    end
    
    methods(Static)
        function obj = fromOrder(tick, action, order)
%             obj = ActionLite;
%             obj.tick=tick;
%             obj.action=action;
%             obj.isBuy=order.IsLong;
%             obj.isOpen=order.IsOpen;
%             obj.price=order.Price;
%             obj.amount=order.Amount;
            obj = table(tick, action, order.IsLong, order.IsOpen, order.Price, order.Amount);
            obj.Properties.VariableNames = {'tick', 'action', 'IsLong', 'IsOpen', 'Price', 'Amount'};
        end
        
        function obj = fromTrade(tick, action, trade)
%             obj = ActionLite;
%             obj.tick=tick;
%             obj.action=action;
%             obj.isBuy=trade.Order.IsLong;
%             obj.isOpen=trade.Order.IsOpen;
%             obj.price=trade.Price;
%             obj.amount=trade.Amount;
            obj = table(tick, action, trade.Order.IsLong, trade.Order.IsOpen, trade.Price, trade.Amount);
            obj.Properties.VariableNames = {'tick', 'action', 'IsLong', 'IsOpen', 'Price', 'Amount'};
        end
    end
    
end

