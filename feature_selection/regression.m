clc
clear

dataNum=96;

Data = LoadMat_SHFE_CU_Main(dataNum); clear dataNum

% choose the first 4 hours

Data.FilterByHours([21 0],[24 1]);
% Data.FilterByHours(9,15);

% take out the first element 
Data = Data.GetRow(2:Data.Length); 

n = length(Data.Ask);

lag = 1;

y = Data.Mid(lag+1:n) - Data.Mid(1:n-lag);

% get buy and sell amount 
bidPrice = Data.Bid;
askPrice = Data.Ask;
bidSize = Data.BidSize;
askSize = Data.AskSize;
VWAP = Data.VWAP;
volume = Data.Volume;
tickSize = 10;


[~,~,~,~,sizeBuy,sizeSell] = fillRate(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize, 20);


x1 = askSize + bidSize;
x1 = x1(1:end-lag);
x1(x1>500) = 500;

askRatio = askSize./(askSize + bidSize);
bidRatio = bidSize./(askSize + bidSize);

x2 = bidRatio(1:end-lag); 
    
x3 = sizeBuy - sizeSell;
x3 = x3(1:end-lag);
x3(x3>100) = 100;
x3(x3<-100) = -100;

openRatio = Data.OpenVolume./Data.Volume;

x4 = openRatio(1:end-lag);
    
%x5 = mid_mkt1(1:n);  

nanIndex = isnan(x4); 

x = [ones(n-lag,1) x1 x2 x3 x4]; 

x(nanIndex,:) = [];
y(nanIndex,:) = [];

for i = 2:5
    x(:,i) = (x(:,i)-mean(x(:,i)))./std(x(:,i));
end

beta = x\y;




