tic
Data = LoadMat_SHFE('shfe_20160111_cu1603.mat');

% choose the first 4 hours

% Data.FilterByHours([22 0],[24 1]);

% Data.FilterByHours(9,15);

% take out the first element 
Data = Data.GetRow(2:Data.Length);

n = length(Data.Ask);

law = 1;
lbw = 30;

% length of the regression vector is n-law-lbw+1 

y = Data.Mid(law+lbw:end) - Data.Mid(lbw:end-law);

ysign= sign (y);
%ysign (abs(y) <10) =0;


% get buy and sell amount 
bidPrice = Data.Bid;
askPrice = Data.Ask;
midPrice = 0.5*(bidPrice + askPrice);
bidSize = Data.BidSize;
askSize = Data.AskSize;
VWAP = Data.VWAP;
volume = Data.Volume;
tickSize = 10;
n = length(bidPrice);

[~,~,~,~,sizeBuy,sizeSell] = fillRate(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize, 20);

vwap=VWAP;
vwap(volume==0)=nan;
[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, vwap, volume, tickSize );


askRatio = askSize./(askSize + bidSize);
bidRatio = bidSize./(askSize + bidSize);

tradeImb = sizeBuy - sizeSell;
sizeImb = bidSize - askSize;

tradeRatio = sizeBuy ./ (sizeBuy +sizeSell);
tradeRatio((sizeBuy +sizeSell)==0) = 0.5;

sizeRatio = bidSize./(askSize + bidSize);
sizeRatio((askSize + bidSize)==0) = 0.5;

directionOpinion = tradeImb*2./volume;

signal = ysign;

cond = zeros (n, 1);
cond (tradeImb>40 & sizeRatio >0.85 & directionOpinion > 0.85)=1;
cond (tradeImb<-40 & sizeRatio <(1-0.85) & directionOpinion < (1-0.85))=-1;

bidSz = bidSize(lbw:end-law);
askSz = askSize(lbw:end-law);
buySz = sizeBuy(lbw:end-law);
sellSz = sizeSell(lbw:end-law);

predictor = table (tradeImb, tradeRatio, sizeImb, sizeRatio, sizeBuy, sizeSell, bidSize, askSize, directionOpinion);

train=[predictor(lbw:end-law, :), table(signal)];

cond2=cond(lbw:end-law);
reduced = train (cond2~=0,:);

ycond = y(cond2~=0);


%plotconfusion (signal, cond2);
Conf_Mat = confusionmat(signal, cond2)

Conf_Mat = confusionmat(signal(cond2~=0), cond2(cond2~=0))
toc


