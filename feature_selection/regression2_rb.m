clc
clear
tic
Data = LoadMat_SHFE('shfe_20160122_rb1605.mat');

% take out the first element 
Data = Data.GetRow(2:Data.Length);

n = length(Data.Ask);

law = 1;
lbw = 30;

% length of the regression vector is n-law-lbw+1 

y = Data.Mid(law+lbw:end) - Data.Mid(lbw:end-law);

ys= sign (y);

% get buy and sell amount 
bidPrice = Data.Bid;
askPrice = Data.Ask;
midPrice = 0.5*(bidPrice + askPrice);
bidSize = Data.BidSize;
askSize = Data.AskSize;
VWAP = Data.VWAP;
volume = Data.Volume;
tickSize = 1;
n = length(bidPrice);

[~,~,~,~,sizeBuy,sizeSell] = fillRate(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize, 20);

vwap=VWAP;
vwap(volume==0)=nan;
[tradePx, tradeSize, tradeSide] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, vwap, volume, tickSize );


askRatio = askSize./(askSize + bidSize);
bidRatio = bidSize./(askSize + bidSize);

% tradeImb
weight_tradeImb = zeros(lbw,1);
weight_tradeImb(1:4) = [0.4 0.3 0.2 0.1];
tradeImb = sizeBuy - sizeSell;
weightedTradeImb = filter(weight_tradeImb,1,tradeImb);
weightedTradeImb = weightedTradeImb(lbw:end-law);

weightedTradeImb(weightedTradeImb>100) = 100;
weightedTradeImb(weightedTradeImb<-100) = -100;

% opinion direction
directionOpinion = tradeImb*2./volume;
directionOpinion(isnan(directionOpinion)) = 0;
weighteddirectionOpinion = filter(weight_tradeImb,1,directionOpinion);
weighteddirectionOpinion = weighteddirectionOpinion(lbw:end-law);


% sizeImb
sizeImb = bidRatio - 0.5;
sizeImb = sizeImb(lbw:end-law);

% sizeImb2
sizeImb2 = bidSize - askSize;
sizeImb2 = sizeImb2(lbw:end-law);

% mkt indicator
trendingMKT = zeros(n-law-lbw+1,1);
for i = 1:n-law-lbw
    [~ ,trendingMKT(i)] = MKT2(midPrice(i:i+lbw-1));
end

% moving average difference 
ma_short_lag = lbw/2; %lbw has to be an even number 
ma_long_lag  = lbw;
a = 1;
b = ones(1,ma_short_lag)*1/ma_short_lag;
ma_short = filter(b,a,midPrice);

b = ones(1,ma_long_lag)*1/ma_long_lag;
ma_long = filter(b,a,midPrice);

ma_diff = ma_short - ma_long;

ma_diff = ma_diff(lbw:end-law);


% Bollinger bands position
BBP = (midPrice - ma_long);
BBP = BBP(lbw:end-law);
 

midQuantile = zeros(n-law-lbw+1,1);
%mid's quantile of past historically traded prices
for i = 1:n-law-lbw
    trdPx=reshape(tradePx(i:i+lbw-1,:),[],1);
    trdSz=reshape(tradeSize(i:i+lbw-1,:),[],1);
    trdPx=trdPx(~isnan(trdSz));
    trdSz=trdSz(~isnan(trdSz));
    prices=[];
    for j=1:length(trdPx)
        prices = [prices trdPx(j)*ones(1, int32(trdSz(j)))];
    end

    curMidPx = midPrice(i+lbw-1);
    if ~isnan(curMidPx) && ~isempty(prices)
        midQuantile(i) = (invquantile (prices, curMidPx)-0.5)/0.5;
    end
    %trdPx=trdPx(~isnan(trdPx));
    %if ~isnan(curMidPx) && ~isempty(trdPx)
    %    midQuantile(i) = (invprctile (trdPx, midPrice(i+lbw-1))-50)/50;
    %end
end


% regression

tradeImb = tradeImb(lbw:end-law);
x1 = (tradeImb - mean(tradeImb))/std(tradeImb);
x2 = (weightedTradeImb - mean(weightedTradeImb))/std(weightedTradeImb);
x3 = (weighteddirectionOpinion - mean(weighteddirectionOpinion))/std(weighteddirectionOpinion);
x4 = (sizeImb - mean(sizeImb))/std(sizeImb);
x5 = (trendingMKT - mean(trendingMKT))/std(trendingMKT);
x6 = (ma_diff - mean(ma_diff))/std(ma_diff);
x7 = (BBP - mean(BBP))/std(BBP);
x8 = (midQuantile -mean(midQuantile))/std(midQuantile);
x9 = (sizeImb2 - mean(sizeImb2))/std(sizeImb2);

x = [tradeImb weightedTradeImb weighteddirectionOpinion sizeImb trendingMKT ma_diff BBP midQuantile sizeImb2];
x_norm = [x1 x2 x3 x4 x5 x6 x7 x8 x9];

xy= [x_norm y];
xys= [x ys];
xnormys= [x_norm ys];

[b,bint,r,rint,stats]  = regress(y,x_norm);

[coeff,score,latent,tsquared,explained] = pca(x_norm);

pcax1 = x_norm*coeff(:,1);
pcax2 = x_norm*coeff(:,2);
pcax3 = x_norm*coeff(:,3);
pcax4 = x_norm*coeff(:,4);

xx = [ pcax1 pcax2 pcax3 pcax4];


[b,bint,r,rint,stats]  = regress(y,xx);

toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% regression
% y  : increment of mid price over the next 5, 30, 120, 600 ticks
% x1 : tradeImb  
% x2 : weighted tradeImb (integer)
% x3 : opinion direction: tradeImb*2/volume, or weighted opinion direction (range from -1 to 1) 
% x4 : sizeImb, this is only meaningful for a single period, weighted over a lbw does not make much sense 
% x5 : trending indicators of lbw
% x6 : moving average difference
% x7 : Bollinger bands position
% x8 : open vol change during the lbw (open ratio shows that if people are taking more risk) 
% x9: sizeImb2, bidSize - askSize
% x10 : relative position in the support and resistance level

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

