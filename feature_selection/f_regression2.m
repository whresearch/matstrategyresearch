function [ out ] = f_regression2( law )

Data = LoadMat_SHFE('shfe_20160108_cu1603.mat');

% choose the first 4 hours

% Data.FilterByHours([22 0],[24 1]);

% Data.FilterByHours(9,15);

% take out the first element 
Data = Data.GetRow(2:Data.Length);

n = length(Data.Ask);

lbw = 30;

% length of the regression vector is n-law-lbw+1 

y = Data.Mid(law+lbw:end) - Data.Mid(lbw:end-law);

% get buy and sell amount 
bidPrice = Data.Bid;
askPrice = Data.Ask;
midPrice = 0.5*(bidPrice + askPrice);
bidSize = Data.BidSize;
askSize = Data.AskSize;
VWAP = Data.VWAP;
volume = Data.Volume;
tickSize = 10;
n = length(bidPrice);

[~,~,~,~,sizeBuy,sizeSell] = fillRate(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize, 20);

askRatio = askSize./(askSize + bidSize);
bidRatio = bidSize./(askSize + bidSize);

% prepare tradeImb
weight_tradeImb = zeros(lbw,1);
weight_tradeImb(1:4) = [0.4 0.3 0.2 0.1];
tradeImb = sizeBuy - sizeSell;
weightedTradeImb = filter(weight_tradeImb,1,tradeImb);
weightedTradeImb = weightedTradeImb(lbw:end-law);

weightedTradeImb(weightedTradeImb>100) = 100;
weightedTradeImb(weightedTradeImb<-100) = -100;

% prepare opinion direction
directionOpinion = tradeImb*2./volume;
directionOpinion(isnan(directionOpinion)) = 0;
weighteddirectionOpinion = filter(weight_tradeImb,1,directionOpinion);
weighteddirectionOpinion = weighteddirectionOpinion(lbw:end-law);


% prepare sizeImb
sizeImb = bidRatio - 0.5;
sizeImb = sizeImb(lbw:end-law);


% prepare mkt indicator
trendingMKT = zeros(n-law-lbw+1,1);
for i = 1:n-law-lbw
    [~ ,trendingMKT(i)] = MKT2(midPrice(i:i+lbw-1));
end

% do regression

tradeImb = tradeImb(lbw:end-law);
x1 = (tradeImb - mean(tradeImb))/std(tradeImb);
x2 = (weightedTradeImb - mean(weightedTradeImb))/std(weightedTradeImb);
x3 = (weighteddirectionOpinion - mean(weighteddirectionOpinion))/std(weighteddirectionOpinion);
x4 = (sizeImb - mean(sizeImb))/std(sizeImb);
x5 = (trendingMKT - mean(trendingMKT))/std(trendingMKT);


x = [  x1 x2 x3 x4 x5];
pcax = pca(x);

pcax1 = x*pcax(:,1);
pcax2 = x*pcax(:,2);
pcax3 = x*pcax(:,3);
pcax4 = x*pcax(:,4);

xx = [pcax1 pcax2];

beta = xx\y;
out = beta(1)/sum(beta);
end

