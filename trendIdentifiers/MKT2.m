function [ tau, tau2 ] = MKT2(x)

    countUp = 0 ;
    countDown = 0;
    countEq = 0;

    n = length(x);

    for i = 1 : n-1
        for j = i + 1 : n
            if x(j) > x(i)
                countUp = countUp + 1;
            elseif x(j) < x(i)
                countDown = countDown + 1;
            else
                countEq = countEq + 1;
            end
        end
    end

    D = n*(n-1)/2;
    tau = (countUp - countDown)/D;
    
    
    tau2 = countUp/(countUp+countDown) - 0.5;
   
    if isnan(tau2)
        tau2 = 0;
    end
    
end




        



