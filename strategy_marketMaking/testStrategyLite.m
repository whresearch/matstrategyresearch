clc
clear
close all

tic
% load data
% StartDate = datetime(2015, 9, 10);
% EndDate = datetime(2015, 9, 10);
% Data = LoadMat_SHFE_Range('cu1511', StartDate, EndDate);

Data = LoadMat_SHFE_CU_Main(135);

% create strategy
% Strategy = intradayRevLite();

Strategy = marketMakingLite(0.88, 80, 10, 30);
% calculate results
Btl = BacktestLite(Data, Strategy);

Btl.run();

Btl.showResults();

plot(cumsum(Btl.pnlVec));

%tableViewLite
toc
