classdef marketMakingLite < StrategyLite
    
    properties

        % strategy parameters 
        lbw                  = 30;
        
        askSizes             = nan(4,1);
        bidSizes             = nan(4,1);
        
        
        tradeAmountUp            
        tradeAmountDown           
        tradeAmountUnknown 
        preAsk               = nan;
        preBid               = nan;
        
        signalThreshold      = 70;
        
        takeprofitLevel      = 600;
        stoplossLevel        = -600;
        maxHoldingPeriod1    = 1;
        maxHoldingPeriod2    = 120;;
        maxPendingPeriod     = 20;
        openRatioThreshold   = 0.3;   
        askRatioThreshold    = 0.6;
        minTradedVolume      = 3;

        tradeImbThreshold;
        sizeImbThreshold;

        
        
        tickSize = 10;
        
        limitClosePrice
        % storage
        values
        snapshot
        
        % states and control parameters 
        liveAmount           
        positionHoldingTime  
        pendingOrderTime     
        
        fillPrice;
        Last
        
        curStatus; % 0: nothing; 1: has pending order; 2: has position; 3: has position and pending order
        enterType = 0 ;                  % 0 for ioc; 1 for limit
        
        
        pnl = 0;
        cumPnl = 0;

    end
    
    methods

        function this = marketMakingLite(sizeImbThreshold, tradeImbThreshold, maxHoldingPeriod1, maxHoldingPeriod2)
            this.sizeImbThreshold = sizeImbThreshold;
            this.tradeImbThreshold = tradeImbThreshold;
            this.maxHoldingPeriod1 = maxHoldingPeriod1;
            this.maxHoldingPeriod2 = maxHoldingPeriod1+maxHoldingPeriod2;


            this.liveAmount = 0;
            this.positionHoldingTime = 0;
            this.pendingOrderTime = 0;
            this.fillPrice = 0;
            this.curStatus = status.hasNothing;
       
            this.values = NaN(this.lbw, 1);
            
            this.lbw = 30;
            this.tradeAmountUp = zeros(this.lbw,1);  
            this.tradeAmountDown = zeros(this.lbw,1);  
            this.tradeAmountUnknown = zeros(this.lbw,1);  
            
        end
        
        function OnInit( this, InstrumentID, Contract, TradingDay )
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, snapshot )
            
            this.snapshot = snapshot;
            sizeBuy = 0;
            sizeSell = 0;
            sizeUnknown = 0;
            
            if ~isnan(this.preAsk) && ~isnan(this.preBid)
                if isnan(snapshot.VWAP)
                    snapshot.VWAP = 0;
                end
                [sizeBuy, sizeSell, sizeUnknown] = tradeVolByVwap2(this.preBid, this.preAsk, snapshot.VWAP, snapshot.Volume, this.tickSize);
               
                %display([datestr(snapshot.Time),' sizebuy: ', num2str(sizeBuy),' sizesell: ', num2str(sizeSell),' sizeunknown: ',num2str(sizeUnknown)]);
            end
            
            this.preAsk = snapshot.Ask;
            this.preBid = snapshot.Bid;
            
            this.tradeAmountUp = [this.tradeAmountUp(2:end);sizeBuy];
            this.tradeAmountDown = [this.tradeAmountDown(2:end);sizeSell];
            this.tradeAmountUnknown = [this.tradeAmountUp(2:end);sizeUnknown];
            
            signal = this.getSignal();
            
            
            
            switch this.curStatus
                
                case status.hasNothing  
                    
                    if ready2trade(this)
                        
                        % up signal                
                        if true

    
                                display([datestr(this.snapshot.Time),' follow up trades, limit IN! ']);
                                this.BuyOpen(InstrumentID, 1, snapshot.Bid(1), false); 
                           


                        % down signal 
                        elseif signal == -1

             
                                display([datestr(this.snapshot.Time),' follow down trades, limit IN ']);
                                this.SellOpen(InstrumentID, 1, snapshot.Ask(1), false); 
                          

                        end
                        
                    end
                  
                        
                case status.hasOnePendingOrder
                    
                    this.pendingOrderTime =  this.pendingOrderTime + 1;
                    
                    if this.pendingOrderTime >= this.maxPendingPeriod 
                        
                        %%%display([datestr(this.snapshot.Time),' pending order max life reached ']);
                        
                        this.CancelAllOrder();

                    end
    
                case status.hasOnePosition
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                        %disp(['position holding time is ',num2str(this.positionHoldingTime)]);
                    
                    % time filter 
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 1: limit close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                            this.limitClosePrice = snapshot.Ask(1);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter 1: limit close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                            this.limitClosePrice = snapshot.Bid(1);
                            
                        end
                        
                        this.curStatus = 3;
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    % pnl filter 
                    if this.liveAmount > 0
                        pnl = snapshot.Bid(1) - this.fillPrice;
                    elseif this.liveAmount < 0
                        pnl = this.fillPrice - snapshot.Ask(1);
                    end
                    
                    
                    if this.liveAmount > 0 && pnl >= this.takeprofitLevel
                        
                            display([datestr(this.snapshot.Time),' take-profit: IOC close long position']);
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl >= this.takeprofitLevel
                            
                            display([datestr(this.snapshot.Time),' take-profit: IOC close short position']);
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    elseif this.liveAmount > 0 && pnl <= this.stoplossLevel
                        
                            display([datestr(this.snapshot.Time),' stop-loss: IOC close long position']);
                            this.CancelAllOrder();
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                    elseif this.liveAmount < 0 && pnl <= this.stoplossLevel
                            
                            display([datestr(this.snapshot.Time),' stop-loss: IOC close short position']);
                            this.CancelAllOrder();
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                    end
                       
                    
                case status.hasOnePositionOnePendingOrder 
                    
                    if snapshot.Volume > 0
                        this.positionHoldingTime = this.positionHoldingTime + 1;
                    end
                   
                    if this.positionHoldingTime >= this.maxHoldingPeriod1 && this.positionHoldingTime < this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                            
                            if this.limitClosePrice ~= snapshot.Ask(1);
                                
                                display([datestr(this.snapshot.Time),' update limit close long order! ']);
                                
                                this.CancelAllOrder();

                                this.SellClose(InstrumentID, 1, snapshot.Ask(1), false);
                            
                                this.limitClosePrice = snapshot.Ask(1);
                            end
                            
                        elseif this.liveAmount < 0 
                            
                            if this.limitClosePrice ~= snapshot.Bid(1);
                                
                                display([datestr(this.snapshot.Time),'  update limit close short order! ']);
                                
                                this.CancelAllOrder();
                                
                                this.BuyClose(InstrumentID, 1, snapshot.Bid(1), false);
                            
                                this.limitClosePrice = snapshot.Bid(1);
                            end
                            
                        end
                        
                    
                    elseif this.positionHoldingTime >= this.maxHoldingPeriod2
                        
                        if this.liveAmount > 0 
                        	this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter 2 : IOC close long position']);
                            
                            this.SellClose(InstrumentID, 1, snapshot.Bid(1), true);
                            
                        elseif this.liveAmount < 0 
                            this.CancelAllOrder();
                            display([datestr(this.snapshot.Time),' time filter2 : IOC close short position']);
                            
                            this.BuyClose(InstrumentID, 1, snapshot.Ask(1), true);
                            
                        end
                        
                    end
                    
                    
            end
            
        end
            
       

        
        function OnOrderInsertAck( this, Order )
            
            if this.curStatus == status.hasNothing
               
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.pendingOrderTime = 0;
                    this.curStatus = status.hasOnePendingOrder;
                end
                
            elseif this.curStatus == status.hasOnePosition
                
                if Order.IsIOC 
                    %display([datestr(this.snapshot.Time),' IOC order ack ']);
                else
                    %%%display([datestr(this.snapshot.Time),' limit order ack ']);
                    this.pendingOrderTime = 0;
                    this.curStatus = status.hasOnePositionOnePendingOrder;
                end
            else
                error(' status transition error! ');
            end
            
        end
        
        function OnOrderCancel( this, Order )
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    %%%display([datestr(this.snapshot.Time),' IOC IN failure ']);
                    
                case status.hasOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit IN order cancelled ']);
                    
                    this.curStatus = status.hasNothing;
                    
                case status.hasOnePosition
                    
                    %%%display([datestr(this.snapshot.Time),' IOC OUT failure ']);
                    
                case status.hasOnePositionOnePendingOrder
                    
                    %%%display([datestr(this.snapshot.Time),' limit OUT order cancelled ']);
                    
                    this.curStatus = status.hasOnePosition;
                    
            end  
            
        end
        
        function OnTrade( this, Order, Amount, Price )
            
            if Order.IsLong
                this.liveAmount = this.liveAmount + Amount;
            else
                this.liveAmount = this.liveAmount - Amount;
            end
            
            switch this.curStatus
                
                case status.hasNothing
                    
                    display([datestr(this.snapshot.Time),' IOC IN order filled at price ',num2str(Order.Price)]);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePendingOrder
                    
                    display([datestr(this.snapshot.Time),' limit IN order filled']);
                    
                    this.fillPrice = Order.Price;
                    
                    this.positionHoldingTime = 0;
                    
                    this.pendingOrderTime = 0;    
                    
                    this.curStatus = status.hasOnePosition;
                    
                case status.hasOnePosition
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' IOC OUT success at price ',num2str(Order.Price) ,' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                   
                case status.hasOnePositionOnePendingOrder
                    
                    if Order.IsLong
                        this.pnl = this.fillPrice - Price; this.cumPnl = this.cumPnl + this.pnl;
                    else
                        this.pnl = Price - this.fillPrice; this.cumPnl = this.cumPnl + this.pnl;
                    end
                    
                    display([datestr(this.snapshot.Time),' limit OUT success ', ' ............... ',num2str(this.pnl),'/',num2str(this.cumPnl)]);
                    
                    this.curStatus = status.hasNothing;
                    
                    this.fillPrice = 0;
                    
            end

        end
        
        
        function [signal, tradeImb, sizeImb]= getSignal(this)
            
            tradeImb = this.tradeAmountUp(end) - this.tradeAmountDown(end);
            
            sizeImb = this.snapshot.BidSize / (this.snapshot.BidSize + this.snapshot.AskSize);
            
%             if this.snapshot.BidSize < 3 || this.snapshot.AskSize < 3
%                 sizeImb = 0.5; % no signal in this case 
%             end
            
            if tradeImb > this.tradeImbThreshold && sizeImb > this.sizeImbThreshold
                signal = 1;
            elseif tradeImb < -this.tradeImbThreshold && sizeImb < (1-this.sizeImbThreshold)
                signal = -1;    
            else    
                signal = 0;        
            end
                
        end
        
       
        function ret = ready2trade(this)
            ret = true;
%             if isnan(this.askSizes) || isnan(this.bidSizes)
%                 ret = false;
%             end              
        end
        
    end
    
end