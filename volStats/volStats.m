for i = 1:137
Data = LoadMat_SHFE_CU_Main(i);

%Data = LoadMat_SHFE('shfe_20160106_cu1603.mat');
Data.Volume(isnan(Data.Volume)) = 0;

totalVol =  sum(Data.Volume);
Data.FilterByHours([21 0],[24 1]);
nightVol = sum(Data.Volume);
Data.DayInfo.Day
disp([ 'nightVol:',num2str(nightVol), ' totalVol:',num2str(totalVol), '   nightVol ratio: ',num2str(nightVol/totalVol)]);
end