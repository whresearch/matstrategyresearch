clc
clear
close all

%Data = LoadMat_SHFE_CU_Main(1);

returns = zeros(24,1);

for dataNum = 1 : 24

    data = LoadMat_SHFE_CU_Main(dataNum);
    
    returns(dataNum) = data.Last(end)/data.Last(1) - 1;
        
end

clear data