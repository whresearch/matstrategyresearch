clc
clear
close all

tic

Data = LoadMat_SHFE('shfe_20160310_cu1605.mat');

sa_table = get_sa_table(Data);

[ time, tickSize, bidSize, bidPrice, askPrice, mid, askSize, volume, VWAP, last ] = PopOut( Data );

plot(Data.Last)

figure
hold on
[ impactMean, impactWinSelected ] = priceImpact(askPrice-bidPrice==20 , mid, 2000);
[ impactMean2, impactWinSelected2 ] = priceImpact(askPrice-bidPrice==20 & bidSize./(bidSize+askSize) > 0.85 , mid, 2000);
[ impactMean3, impactWinSelected3 ] = priceImpact(askPrice-bidPrice==20 & bidSize./(bidSize+askSize) < 0.15 , mid, 2000);
plot(impactMean)

plot(impactMean2)
plot(impactMean3)
hold off