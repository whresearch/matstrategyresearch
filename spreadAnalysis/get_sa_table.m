function output_table = get_sa_table(Data)

    Data = LoadMat_SHFE('shfe_20160317_cu1605.mat');

    [ time, tickSize, bidSize, bidPrice, askPrice, mid, askSize, volume, VWAP, last ] = PopOut( Data );

    bidRatio = Data.BidSize./(Data.AskSize + Data.BidSize);

    [sizeBuy,sizeSell] = tradeDirection(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

    sizeBuy=int32(sizeBuy);
    sizeSell=int32(sizeSell);
    tradeImb = sizeBuy - sizeSell;
   
    [tradePx, tradeSize, ~] = tradeVolByVwap(bidPrice, askPrice, bidSize, askSize, VWAP, volume, tickSize);

    [passiveOrderBid, passiveOrderAsk] = passiveLiquidityVec (bidPrice, askPrice, bidSize, askSize, tradePx, tradeSize);

    passiveLiquidity = passiveOrderBid - passiveOrderAsk;

    spread = (askPrice - bidPrice)/tickSize;
    
    spreadCondition = spread == 2;
    singleVol = volume*0.5;
    output_table = table(time, bidSize, bidPrice, askPrice, askSize, bidRatio, tradeImb, sizeBuy, sizeSell,tradeSize,VWAP, passiveLiquidity, spreadCondition);

end
