% size distribution during different hours 

startDate = 120;

num_dates = 2;

means = zeros(num_dates,1);

for dataNum = startDate : startDate+num_dates-1
    
    Data = LoadMat_SHFE_CU_Main(dataNum); 

    %Data.FilterByHours(14.5,15);

    means(dataNum-startDate+1) = mean(Data.AskSize-Data.BidSize);

end

mean(means)

% 
% num_bins = 16;
% 
% n = length(bidSize);
% 
% m = round(n/num_bins)-1;
% 
% avg=zeros(num_bins,1);
% 
% for i = 1:num_bins
%     
%     avg(i) = mean(bidSize( (i-1)*m+1 : i*m ) );
% 
% end

% day 110-135:  avg_bidSize  avg_askSize 
% 21  - 22        82          80
% 22  - 23        82          77      
% 23  - 24        75          74
% 0   - 1         66          63

% 9   - 10        87          84
% 10  - 11        93          88
% 11  - 11.5      95          89
% 
% 1.5 - 2         89          93
% 2   - 2.5       96          92
% 2.5 - 3         89          86