clc
clear
% Load data 
tic

Data = LoadMat_SHFE_CU_Main(80);

prob = 0.95;

x = Data.VWAP;
n = length(x);
for i = 1:n
    if(isnan(x(i)))
        x(i)=x(i-1);
    end
end

lbw = 100;

taus = [];

for i = lbw : n
    
    tau = MKT2(x(i-lbw+1:i));
    
    taus = [taus; tau];

end

quantile(taus,prob)

quantile(taus,1-prob)

toc