clc
clear
% Load data 
tic

day = 80;
mean_inc = [];
num_cases = [];
for day = 50:51
Data = LoadMat_SHFE_CU_Main(day);

x = Data.Mid;

n = Data.Length;

lbw = 100;

law = 200;

mktThreshold = 0.67;

incr = [];
numCases =0;
for i = lbw : n-law
  
    tau = MKT2(x(i-lbw+1:i));
    
    if tau > mktThreshold
        
        incr = [incr ; x(i+law)-x(i)];
        numCases = numCases + 1;
    end

end
    
mean_inc = [mean_inc;mean(incr)];
num_cases = [num_cases;numCases];
end
toc
