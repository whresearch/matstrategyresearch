close all
clc
clear
% Load data 
tic

Data1 = cell(3);
n = 5;
for k = 1:n
    Data1{k} = LoadMat_SHFE_CU_Main(80+k);
end

figure 

%parpool('local',4);

for i = 1 : n
    
    Data = Data1{i};
    % Data.FilterByPrice('Mid');
    % Calculate Mid-Price
    mid = (Data.Bid + Data.Ask) / 2;

    threshold_base = 2;
    threshold_sizeRatio=0.6;
    threshold_volRatio=0.65;
    law = 1; %look ahead window
    k=150;
    mean_inc=zeros(k,1);

    for law = 1:k
        
        increment=[];
        for j =  2 :  Data.Length - law

            Vol = Data.Volume(j);
            OI = Data.OpenInterest(j) - Data.OpenInterest(j - 1);
            ORatio = ((Vol + OI) / 2 )/ Vol;
            %&&  (ORatio > threshold_volRatio)
            if Data.AskSize(j) > threshold_base && Data.BidSize(j) > threshold_base ...
               && Data.BidSize(j)/(Data.AskSize(j)+Data.BidSize(j)) > threshold_sizeRatio ...
               && ORatio > threshold_volRatio
              increment=[increment;mid(j+law)-mid(j)];

            end
        end

        mean_inc(law) = mean(increment)';
    end

    plot(mean_inc);
    hold on
end
toc 
    
%delete(gcp);
    
    
%parameters to optimize: ratios , threshold_base, find stable law across
%different days 